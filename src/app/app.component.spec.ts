import { TestBed, async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';

import { CoreModule } from './modules/core/core.module';
import { SharedModule } from './modules/shared/shared.module';
import { DashboardModule } from './modules/dashboard/dashboard.module';

import { StudentDataService } from './modules/core/services/student-data.service';
import { RESTService } from './modules/core/services/rest.service';

describe('AppComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent
            ],
            imports: [
                BrowserModule,
                FormsModule,
                HttpModule,
                CoreModule,
                SharedModule,
                DashboardModule,
            ],
            providers: [
                StudentDataService,
                RESTService
            ],
        });
        TestBed.compileComponents();
    });

    it('should create the app', (() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

    it(`should have as component 'gl-dashboard'`, (() => {
        const fixture = TestBed.createComponent(AppComponent);
        const dashboard = fixture.debugElement.query(By.css('gl-dashboard'));
        expect(dashboard).toBeTruthy();
    }));
});
