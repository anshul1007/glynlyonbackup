﻿export const Constants = {
    baseUrl: 'http://ec2-54-183-189-48.us-west-1.compute.amazonaws.com:9090/api/',
    url: {
        getAssingnmentCompletion: 'teacher/assignments/progress',
        getCurrentScore: 'teacher/assignments/score',
        getStudentBreakdown: 'teacher/assignments',
        getLastRefreshed: 'datastore/refresh',
        getStudentDetail: 'teacher/assignments/progress',
        dashboardCSV: 'export/assignments/csv',
        getUserRole: 'user/details',
        getCampusTeacherList: 'campus/teacher/list'
    }
};
