import { TestBed, async, inject } from '@angular/core/testing';
import {  StudentDataService } from './student-data.service';

import { HttpModule } from '@angular/http';

import { RESTService } from '../services/rest.service';

describe('StudentDataService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                StudentDataService,
                RESTService
            ],
            imports: [
                HttpModule
            ]
        });
    });

    it('should ...', inject([StudentDataService], (service: StudentDataService) => {
        expect(service).toBeTruthy();
    }));
});
