﻿import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions, ResponseContentType} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';

import { ErrorInfo } from '../../../common/models/error-info.model';

@Injectable()
export class RESTService {
    /* tslint:disable:max-line-length */

    get token(): string {
        if (!localStorage['token']) {
            localStorage['token'] = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI0MDhhZTg5OC05NTVjLTRhM2MtOGVmZS0yNTM4Y2QwMGJjMzciLCJpc3MiOiJvdy12MiIsImV4cCI6MTU0NDU3MjgwMCwiYXVkIjoiZGFzaGJvYXJkIiwiZ2xfY3VzdG9tIjp7InN1Yl9kaXNwbGF5X25hbWUiOiJUZWFjaGVyLCBUZWFjaGVyIiwicm9sZV9pbl9pc3N1ZXIiOiJURUFDSEVSIiwiaXNzdWVyX2Jhc2VfdXJsIjoiaHR0cDovL3N0cmVhbTEtZGV2LW93c2Nob29sLm93dGVhbS5jb206ODAvb3dzb28iLCJzY2hvb2xfdXVpZCI6IjdkYTAzYTc3LWE2ZTgtNDE5NS1iNzg2LTY4NTZiOTNlMmU3ZiIsImRhdGFzb3VyY2UiOiJzdHJlYW0xZGV2In19.OWIp0ogpKHEtf56-SKY2t_ejYdVzYBK8cZCjY9m6-QA';
        }
        return localStorage['token'];
    }
    ////set token(value: string) {
    ////    localStorage["token"] = value;
    ////}

    ////private token = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI0MDhhZTg5OC05NTVjLTRhM2MtOGVmZS0yNTM4Y2QwMGJjMzciLCJpc3MiOiJvdy12MiIsImV4cCI6MTU0NDU3MjgwMCwiYXVkIjoiZGFzaGJvYXJkIiwiZ2xfY3VzdG9tIjp7InN1Yl9kaXNwbGF5X25hbWUiOiJUZWFjaGVyLCBUZWFjaGVyIiwicm9sZV9pbl9pc3N1ZXIiOiJURUFDSEVSIiwiaXNzdWVyX2Jhc2VfdXJsIjoiaHR0cDovL3N0cmVhbTEtZGV2LW93c2Nob29sLm93dGVhbS5jb206ODAvb3dzb28iLCJzY2hvb2xfdXVpZCI6IjdkYTAzYTc3LWE2ZTgtNDE5NS1iNzg2LTY4NTZiOTNlMmU3ZiIsImRhdGFzb3VyY2UiOiJzdHJlYW0xZGV2In19.OWIp0ogpKHEtf56-SKY2t_ejYdVzYBK8cZCjY9m6-QA';
    ////private token = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI0MDhhZTg5OC05NTVjLTRhM2MtOGVmZS0yNTM4Y2QwMGJjMzciLCJpc3MiOiJvdy12MiIsImV4cCI6MTU0NDU3MjgwMCwiYXVkIjoiZGFzaGJvYXJkIiwiZ2xfY3VzdG9tIjp7InN1Yl9kaXNwbGF5X25hbWUiOiJUZWFjaGVyLCBUZWFjaGVyIiwicm9sZV9pbl9pc3N1ZXIiOiJBRE1JTiIsImlzc3Vlcl9iYXNlX3VybCI6Imh0dHA6Ly9zdHJlYW0xLWRldi1vd3NjaG9vbC5vd3RlYW0uY29tOjgwL293c29vIiwic2Nob29sX3V1aWQiOiI3ZGEwM2E3Ny1hNmU4LTQxOTUtYjc4Ni02ODU2YjkzZTJlN2YiLCJkYXRhc291cmNlIjoic3RyZWFtMWRldiIsInRpbWV6b25lIjoiQW1lcmljYS9OZXdfWW9yayJ9fQ.pKwHTg4xcsWQcAJs-kQ9VamMYdXGDjHiUhHGgeV2UY4';

    /* tslint:enable:max-line-length */

    constructor(private http: Http) {

    }

    public get(url: string): Observable<any> {
        const headers = new Headers();
        headers.append('Authorization', this.token);
        return this.http.get(url, {
            headers: headers
        })
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    public getFile(url: string): Observable<any> {
        const headers = new Headers();
        headers.append('Authorization', this.token);
        return this.http.get(url, { headers: headers })
            .map((res: Response) => res)
            .catch(this.handleError);
    }

    public getPDFFile(url: string, body: any): Observable<any> {
        const headers = new Headers();
        headers.append('Authorization', this.token);
        return this.http.post(url, body, { headers: headers, responseType: ResponseContentType.Blob })
            .map((res: Response) => res)
            .catch(this.handleError);
    }

    public post(url: string, body: any): Observable<any> {
        return this.http.post(url, body)
            .map((res: Response) => { return res.json(); })
            .catch(this.handleError);
    }

    public patch(url: string, body: any): Observable<any> {
        return this.http.patch(url, body)
            .map((res: Response) => { return res.json(); })
            .catch(this.handleError);
    }

    public put(url: string, body: any): Observable<any> {
        return this.http.put(url, body)
            .map((res: Response) => { return res.json(); })
            .catch(this.handleError);
    }

    public delete(url: string): Observable<any[]> {
        return this.http.get(url)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    private handleError(responseError: any) {
        if (responseError instanceof Response) {
            let errInfo: ErrorInfo;
            errInfo = {
                content: responseError.json(),
                httpStatus: responseError.status,
                httpStatusText: responseError.statusText
            };

            return Observable.throw(errInfo);
        }
        return Observable.throw(responseError || 'server error');
    }
}
