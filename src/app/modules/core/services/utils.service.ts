﻿import { Injectable } from '@angular/core';

@Injectable()
export class Utils {
    static downloadCsvFile(fileName, csvContent, exporterOlderExcelCompatibility) {
        const D = document;
        const a = D.createElement('a');
        const strMimeType = 'application/octet-stream;charset=utf-8';
        let rawFile;
        let ieVersion;

        ieVersion = this.isIE();
        if (ieVersion && ieVersion < 10) {
            const frame = D.createElement('iframe');
            document.body.appendChild(frame);

            frame.contentWindow.document.open('text/html', 'replace');
            frame.contentWindow.document.write('sep=,\r\n' + csvContent);
            frame.contentWindow.document.close();
            frame.contentWindow.focus();
            frame.contentWindow.document.execCommand('SaveAs', true, fileName);

            document.body.removeChild(frame);
            return true;
        }

        // IE10+
        if (navigator.msSaveBlob) {
            return navigator.msSaveOrOpenBlob(
                new Blob(
                    [exporterOlderExcelCompatibility ? '\uFEFF' : '', csvContent],
                    { type: strMimeType }),
                fileName
            );
        }

        // html5 A[download]
        if ('download' in a) {
            const blob = new Blob(
                [exporterOlderExcelCompatibility ? '\uFEFF' : '', csvContent],
                { type: strMimeType }
            );
            rawFile = URL.createObjectURL(blob);
            a.setAttribute('download', fileName);
        } else {
            rawFile = 'data:' + strMimeType + ',' + encodeURIComponent(csvContent);
            a.setAttribute('target', '_blank');
        }

        a.href = rawFile;
        a.setAttribute('style', 'display:none;');
        D.body.appendChild(a);
        setTimeout(() => {
            if (a.click) {
                a.click();
                // Workaround for Safari 5
            } else if (document.createEvent) {
                const eventObj = document.createEvent('MouseEvents');
                eventObj.initEvent('click', true, true);
                a.dispatchEvent(eventObj);
            }
            D.body.removeChild(a);

        }, 100);
    }

    static downloadPdfFile(fileName, pdfContent) {
        const D = document;
        const a = D.createElement('a');
        const strMimeType = 'application/pdf;charset=utf-8';
        let rawFile;
        let ieVersion;

        ieVersion = this.isIE();
        if (ieVersion && ieVersion < 10) {
            const frame = D.createElement('iframe');
            document.body.appendChild(frame);

            frame.contentWindow.document.open('text/html', 'replace');
            frame.contentWindow.document.write('sep=,\r\n' + pdfContent);
            frame.contentWindow.document.close();
            frame.contentWindow.focus();
            frame.contentWindow.document.execCommand('SaveAs', true, fileName);

            document.body.removeChild(frame);
            return true;
        }

        // IE10+
        if (navigator.msSaveBlob) {
            return navigator.msSaveOrOpenBlob(
                new Blob(
                    [pdfContent],
                    { type: strMimeType }),
                fileName
            );
        }

        // html5 A[download]
        if ('download' in a) {
            const blob = new Blob(
                [pdfContent],
                { type: strMimeType }
            );
            rawFile = URL.createObjectURL(blob);
            a.setAttribute('download', fileName);
        } else {
            rawFile = 'data:' + strMimeType + ',' + encodeURIComponent(pdfContent);
            a.setAttribute('target', '_blank');
        }

        a.href = rawFile;
        a.setAttribute('style', 'display:none;');
        D.body.appendChild(a);
        setTimeout(() => {
            if (a.click) {
                a.click();
                // Workaround for Safari 5
            } else if (document.createEvent) {
                const eventObj = document.createEvent('MouseEvents');
                eventObj.initEvent('click', true, true);
                a.dispatchEvent(eventObj);
            }
            D.body.removeChild(a);

        }, 100);
    }

    static isIE() {
        const match = navigator.userAgent.match(/(?:MSIE |Trident\/.*; rv:)(\d+)/);
        return match ? parseInt(match[1], 10) : false;
    }
}
