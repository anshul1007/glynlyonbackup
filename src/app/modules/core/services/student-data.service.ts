import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';

import { Constants } from '../../../app.constants';
import { RESTService } from '../services/rest.service';
import { FilterCriteria } from '../../../common/models/filter-criteria.model';
import { SortCriteria } from '../../../common/models/sort-criteria.model';

@Injectable()
export class StudentDataService {

    constructor(private glRESTService: RESTService) { }

    public getAssingnmentCompletion(campusId = '', teacherId = ''): Observable<any> {
        let param = '?t=1';
        if (campusId) {
            param = param + '&campusId=' + campusId;
        }
        if (teacherId) {
            param = param + '&teacherId=' + teacherId;
        }
        return this.glRESTService.get(Constants.baseUrl + Constants.url.getAssingnmentCompletion + param)
            .map(res => res)
            .catch(error => error);
    }

    public getCurrentScore(campusId = '', teacherId = ''): Observable<any> {
        let param = '?t=1';
        if (campusId) {
            param = param + '&campusId=' + campusId;
        }
        if (teacherId) {
            param = param + '&teacherId=' + teacherId;
        }
        return this.glRESTService.get(Constants.baseUrl + Constants.url.getCurrentScore + param)
            .map(res => res)
            .catch(error => error);
    }

    public getStudentBreakdown(offset: number, filter: FilterCriteria, sort: SortCriteria, campusId = '', teacherId = ''): Observable<any> {
        let param = '?t=1';
        if (campusId) {
            param = param + '&campusId=' + campusId;
        }
        if (teacherId) {
            param = param + '&teacherId=' + teacherId;
        }
        if (offset > 1) {
            param = param + '&offset=' + offset;
        }
        if (sort) {
            param = param + '&orderBy=' + sort.orderBy + '&sort=' + sort.sort;
        }
        if (filter.overdue) {
            param = param + '&filter=' + filter.overdue;
        }
        if (filter.score) {
            param = param + '&filter=' + filter.score;
        }
        if (filter.student) {
            param = param + '&searchBy=' + filter.student;
        }
        return this.glRESTService.get(Constants.baseUrl + Constants.url.getStudentBreakdown + param)
            .map(res => res)
            .catch(error => error);
    }

    public getLastRefreshed(): Observable<any> {
        const result = [];
        return this.glRESTService.get(Constants.baseUrl + Constants.url.getLastRefreshed)
            .map(res => res)
            .catch(error => error);
    }

    public getStudentDetail(studentId: string, teacherId: string): Observable<any> {
        let param = '?t=1';
        if (teacherId) {
            param = param + '&teacherId=' + teacherId;
        }
        return this.glRESTService.get(Constants.baseUrl + 'user/' + studentId + '/details' + param)
            .map(res => res)
            .catch(error => error);
    }

    public getFiles(filter: FilterCriteria, sort: SortCriteria, campusId, teacherId): Observable<any> {
        let param = '?t=1';
        if (campusId) {
            param = param + '&campusId=' + campusId;
        }
        if (teacherId) {
            param = param + '&teacherId=' + teacherId;
        }
        if (sort) {
            param = param + '&orderBy=' + sort.orderBy + '&sort=' + sort.sort;
        }
        if (filter.overdue) {
            param = param + '&filter=' + filter.overdue;
        }
        if (filter.score) {
            param = param + '&filter=' + filter.score;
        }
        if (filter.student) {
            param = param + '&searchBy=' + filter.student;
        }
        return this.glRESTService.getFile(Constants.baseUrl + Constants.url.dashboardCSV + param)
            .map(res => res)
            .catch(error => error);
    }

    public getDetailCSV(studentId: string, teacherId: string): Observable<any> {
        let param = '?t=1';
        if (teacherId) {
            param = param + '&teacherId=' + teacherId;
        }
        return this.glRESTService.getFile(Constants.baseUrl + '/export/student/' + studentId + '/progress' + param)
            .map(res => res)
            .catch(error => error);
    }

    public getDetailPDF(filter: FilterCriteria, sort: SortCriteria, formData: FormData, campusId, teacherId): Observable<any> {
        let param = '?t=1';
        if (campusId) {
            param = param + '&campusId=' + campusId;
        }
        if (teacherId) {
            param = param + '&teacherId=' + teacherId;
        }
        if (sort) {
            param = param + '&orderBy=' + sort.orderBy + '&sort=' + sort.sort;
        }
        if (filter.overdue) {
            param = param + '&filter=' + filter.overdue;
        }
        if (filter.score) {
            param = param + '&filter=' + filter.score;
        }
        if (filter.student) {
            param = param + '&searchBy=' + filter.student;
        }

        return this.glRESTService.getPDFFile(Constants.baseUrl + 'export/assignments.pdf' + param, formData)
            .map(res => res)
            .catch(error => error);
    }

    public getStudentSubjects(studentId: string, teacherId: string): Observable<any> {
        let param = '?t=1';
        if (teacherId) {
            param = param + '&teacherId=' + teacherId;
        }
        return this.glRESTService.get(Constants.baseUrl + 'student/' + studentId + '/enrollments' + param)
            .map(res => res)
            .catch(error => error);
    }

    public getStudentSBT(studentId: string, subjectId: number, teacherId: string): Observable<any> {
        let param = '?t=1';
        if (teacherId) {
            param = param + '&teacherId=' + teacherId;
        }
        return this.glRESTService.get(Constants.baseUrl + 'student/' + studentId + '/enrollments/' + subjectId + '/scores' + param)
            .map(res => res)
            .catch(error => error);
    }

    public getStudentProgress(studentId: string, subjectId: number, teacherId: string): Observable<any> {
        let param = '?t=1';
        if (teacherId) {
            param = param + '&teacherId=' + teacherId;
        }
        return this.glRESTService.get(Constants.baseUrl + 'student/' + studentId + '/enrollments/' + subjectId + param)
            .map(res => res)
            .catch(error => error);
    }

    public getStudentLogTime(studentId: string, subjectId: number, teacherId: string): Observable<any> {
        let param = '?t=1';
        if (teacherId) {
            param = param + '&teacherId=' + teacherId;
        }
        return this.glRESTService.get(Constants.baseUrl + 'student/' + studentId + '/enrollments/' + subjectId + '/taskstatus' + param)
            .map(res => res)
            .catch(error => error);
    }

    public getUserRole(): Observable<any> {
        return this.glRESTService.get(Constants.baseUrl + Constants.url.getUserRole)
            .map(res => res)
            .catch(error => error);
    }

    public getCampusTeacherList(): Observable<any> {
        return this.glRESTService.get(Constants.baseUrl + Constants.url.getCampusTeacherList)
            .map(res => res)
            .catch(error => error);
    }
}
