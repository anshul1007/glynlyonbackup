import {
    async,
    getTestBed,
    TestBed,
    inject
} from '@angular/core/testing';

import {
    BaseRequestOptions,
    Http,
    Response,
    ResponseOptions,
    XHRBackend
} from '@angular/http';
import {
    MockBackend,
    MockConnection
} from '@angular/http/testing';

import { RESTService } from '../services/rest.service';

describe('RESTService', () => {

    let backendModule: MockBackend;
    let restService: RESTService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                BaseRequestOptions,
                MockBackend,
                RESTService,
                {
                    deps: [
                        MockBackend,
                        BaseRequestOptions
                    ],
                    provide: Http,
                    useFactory: (backend: XHRBackend, defaultOptions: BaseRequestOptions) => {
                        return new Http(backend, defaultOptions);
                    }
                }
            ]
        });

        const testbed = getTestBed();
        backendModule = testbed.get(MockBackend);
        restService = testbed.get(RESTService);
    });

    function setupConnections(backend: MockBackend, options: any) {
        backend.connections.subscribe((connection: MockConnection) => {
            if (connection.request.url === 'api/data') {
                const responseOptions = new ResponseOptions(options);
                const response = new Response(responseOptions);

                connection.mockRespond(response);
            }
        });
    }

    it('should ...', inject([RESTService], (service: RESTService) => {
        expect(service).toBeTruthy();
    }));

    it('should return the data from the server on success', () => {
        setupConnections(backendModule, {
            body: {
                data: [
                    {
                        id: 1,
                        questions: [],
                        title: 'Pizza'
                    },
                    {
                        id: 4,
                        questions: [],
                        title: 'Burrito'
                    },
                    {
                        id: 2,
                        questions: [],
                        title: 'Cheeseburger'
                    }
                ]
            },
            status: 200
        });

        restService.get('api/data').subscribe((data: any) => {
            expect(data.data.length).toBe(3);
        });
    });
});
