﻿import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RESTService } from './services/rest.service';
import { EmitterService } from './services/emitter.service';
import { StudentDataService } from './services/student-data.service';
import { Utils } from './services/utils.service';

@NgModule({
    imports: [CommonModule],
    providers: [
        RESTService,
        EmitterService,
        StudentDataService,
        Utils],

})
export class CoreModule {
    constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
        this.throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }

    throwIfAlreadyLoaded(parentModule: any, moduleName: string) {
        if (parentModule) {
            throw new Error(`${moduleName} has already been loaded. Import Core modules in the AppModule only.`);
        }
    }
}
