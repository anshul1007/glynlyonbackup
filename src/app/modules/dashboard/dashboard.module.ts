﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { StudentDetailsModule } from '../student-detail/student-detail.module';
import { SharedModule } from '../shared/shared.module';

import { DashboardComponent } from './dashboard.component';
import { AssignmentCompletionComponent } from './components/assignment-completion/assignment-completion.component';
import { BarGraphComponent } from './components/bar-graph/bar-graph.component';
import { LastRefreshComponent } from './components/last-refresh/last-refresh.component';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import { ScoreComponent } from './components/score/score.component';
import { StudentBreakdownComponent } from './components/student-breakdown/student-breakdown.component';
import { TableComponent } from './components/table/table.component';

@NgModule({
    imports: [
        BrowserModule, FormsModule, StudentDetailsModule, SharedModule
    ],
    declarations: [
        DashboardComponent,
        AssignmentCompletionComponent,
        PieChartComponent,
        BarGraphComponent,
        TableComponent,
        ScoreComponent,
        StudentBreakdownComponent,
        LastRefreshComponent
    ],
    exports: [
        DashboardComponent
    ],
})

export class DashboardModule {

}
