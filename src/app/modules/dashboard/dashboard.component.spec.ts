import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SharedModule } from '../shared/shared.module';
import { StudentDetailsModule } from '../student-detail/student-detail.module';

import { DashboardComponent } from './dashboard.component';
import { DataErrorComponent } from '../shared/components/data-error/data-error.component';
import { LoadingComponent } from '../shared/components/loading/loading.component';
import { TourComponent } from '../shared/components/tour/tour.component';
import { AssignmentCompletionComponent } from './components/assignment-completion/assignment-completion.component';
import { ScoreComponent } from './components/score/score.component';
import { LastRefreshComponent } from './components/last-refresh/last-refresh.component';
import { StudentBreakdownComponent } from './components/student-breakdown/student-breakdown.component';
import { StudentDetailComponent } from '../student-detail/student-detail.component';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import { BarGraphComponent } from './components/bar-graph/bar-graph.component';
import { TableComponent } from './components/table/table.component';

import { StudentDataService } from '../core/services/student-data.service';
import { EmitterService } from '../core/services/emitter.service';
import { RESTService } from '../core/services/rest.service';
import { Utils } from '../core/services/utils.service';
import { PluralizePipe } from '../shared/pipes/pluralize.pipe';

const DataSetup = {
    userDetails: [{
        "shard": "owsoo3",
        "id": 121298,
        "userId": "dbc224c0-af85-44df-bafd-5e5885fdd667",
        "firstName": "Joe",
        "lastName": "Vanderpool",
        "userType": "ADMIN",
        "status": "ACTIVE",
        "refreshDateTime": 1496997178008
    }]
};

fdescribe('GlAppComponent', () => {
    let component: DashboardComponent;
    let fixture: ComponentFixture<DashboardComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [DashboardComponent, AssignmentCompletionComponent, ScoreComponent, DataErrorComponent,
                LoadingComponent, PieChartComponent, BarGraphComponent, TableComponent,
                StudentBreakdownComponent, TourComponent, LastRefreshComponent, StudentDetailComponent, PluralizePipe],
            providers: [StudentDataService, RESTService, EmitterService, Utils],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should go from model to change event', async(() => {
        const studentDataService = TestBed.get(StudentDataService);
        spyOn(studentDataService, 'getUserRole').and.returnValue(Observable.of(DataSetup.userDetails));
        component.ngOnInit();
        //component.cities = [{ 'name': 'SF' }, { 'name': 'NYC' }, { 'name': 'Buffalo' }];
        //component.selectedCity = comp.cities[1];
        //fixture.detectChanges();
        //const select = fixture.debugElement.query(By.css('select'));

        //fixture.whenStable().then(() => {
        //    dispatchEvent(select.nativeElement, 'change');
        //    fixture.detectChanges();
        //    fixture.whenStable().then(() => {
        //        expect(comp.onSelected).toHaveBeenCalledWith({ name: 'NYC' });
        //        console.log('after expect NYC');
        //    });
        //});
    }));
});
