import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { LastRefreshComponent } from './last-refresh.component';
import { DataErrorComponent } from '../../../shared/components/data-error/data-error.component';
import { LoadingComponent } from '../../../shared/components/loading/loading.component';

import { StudentDataService } from '../../../core/services/student-data.service';
import { EmitterService } from '../../../core/services/emitter.service';
import { RESTService } from '../../../core/services/rest.service';
import { Utils } from '../../../core/services/utils.service';
import { PluralizePipe } from '../../../shared/pipes/pluralize.pipe';


describe('LastRefreshComponent', () => {
    let component: LastRefreshComponent;
    let fixture: ComponentFixture<LastRefreshComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [LastRefreshComponent, DataErrorComponent, LoadingComponent],
            providers: [StudentDataService, RESTService, EmitterService, Utils, PluralizePipe]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LastRefreshComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('sort should call `StudentDataService.getLastRefreshed`', () => {
        const studentDataService = TestBed.get(StudentDataService);
        spyOn(studentDataService, 'getLastRefreshed').and.returnValue(Observable.of([]));
        component.getResult();
        expect(studentDataService.getLastRefreshed).toHaveBeenCalled();
    });
});
