import { Component, OnInit } from '@angular/core';

import { StudentDataService } from '../../../core/services/student-data.service';

@Component({
    selector: 'gl-last-refresh',
    templateUrl: './last-refresh.component.html',
    styleUrls: ['./last-refresh.component.less']
})
export class LastRefreshComponent implements OnInit {

    private lastRefresh;
    private label: string;
    private dateAndTime: string;

    constructor(private getData: StudentDataService) { }

    ngOnInit() {
        this.getResult();
    }

    getResult() {
        this.getData.getLastRefreshed().subscribe(post => {
            this.lastRefresh = post;
            this.label = this.lastRefresh.label;
            this.dateAndTime = this.lastRefresh.dateAndTime;
        });
    }
}
