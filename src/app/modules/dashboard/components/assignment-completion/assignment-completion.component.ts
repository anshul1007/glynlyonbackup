import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';

import { EmitterService } from '../../../core/services/emitter.service';
import { StudentDataService } from '../../../core/services/student-data.service';

@Component({
    selector: 'gl-assignment-completion',
    templateUrl: './assignment-completion.component.html',
    styleUrls: ['./assignment-completion.component.less'],
    changeDetection: ChangeDetectionStrategy.Default
})

export class AssignmentCompletionComponent implements OnInit, OnDestroy {

    private assignmentCompletion: Array<any>;
    private errorOccurred: boolean;
    private isLoading = false;
    private campusId: string = '';
    private teacherId: string = '';
    private getAssingnmentCompletion: any;

    constructor(private getData: StudentDataService, private changeDetectorRef: ChangeDetectorRef) { }

    ngOnInit() {
        this.getResult();
        document.addEventListener('touchstart', function () { }, true);
        EmitterService.get('adminSearch').subscribe(value => this.adminSearch(value));
    }

    private adminSearch(value) {
        this.campusId = value.campusId;
        this.teacherId = value.teacherId;
        this.getResult();
    }

    getResult() {
        this.isLoading = true;
        this.getAssingnmentCompletion = this.getData.getAssingnmentCompletion(this.campusId, this.teacherId).subscribe(
            (data) => {
                const filter = data.filter(x => x.studentCount > 0);
                this.isLoading = false;
                this.errorOccurred = false;
                this.assignmentCompletion = filter;
                ////this.changeDetectorRef.markForCheck();
            },
            (error) => {
                this.isLoading = false;
                this.errorOccurred = true;
                ////this.changeDetectorRef.markForCheck();
            });
    }

    ngOnDestroy() {
        if (this.getAssingnmentCompletion) {
            this.getAssingnmentCompletion.unsubscribe();
        }
    }
}

