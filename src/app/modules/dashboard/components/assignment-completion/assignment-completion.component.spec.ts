import { async, ComponentFixture, TestBed, inject, fakeAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { AssignmentCompletionComponent } from './assignment-completion.component';
import { PieChartComponent } from '../pie-chart/pie-chart.component';
import { DataErrorComponent } from '../../../shared/components/data-error/data-error.component';
import { LoadingComponent } from '../../../shared/components/loading/loading.component';

import { StudentDataService } from '../../../core/services/student-data.service';
import { EmitterService } from '../../../core/services/emitter.service';
import { RESTService } from '../../../core/services/rest.service';
import { Utils } from '../../../core/services/utils.service';
import { PluralizePipe } from '../../../shared/pipes/pluralize.pipe';


describe('AssignmentCompletionComponent', () => {
    let component: AssignmentCompletionComponent;
    let fixture: ComponentFixture<AssignmentCompletionComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [AssignmentCompletionComponent, PieChartComponent, DataErrorComponent, LoadingComponent],
            providers: [StudentDataService, RESTService, EmitterService, Utils, PluralizePipe]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AssignmentCompletionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should have `gl-pie-chat`', fakeAsync((done) => {
        const studentDataService = TestBed.get(StudentDataService);
        spyOn(studentDataService, 'getAssingnmentCompletion').and
            .returnValue(Observable.of([
                { 'label': '5+ Overdue', 'filter': 'overdueplus5', 'overduePercentage': 64, 'studentCount': 35 },
                { 'label': '1-4 Overdue', 'filter': 'overduebetween1to4', 'overduePercentage': 1, 'studentCount': 1 },
                { 'label': '0 Overdue', 'filter': 'overduezero', 'overduePercentage': 33, 'studentCount': 18 }]));
        component.ngOnInit();
        fixture.detectChanges();
        const pieChart = fixture.debugElement.query(By.css('gl-pie-chart')).nativeElement;
        expect(pieChart).toBeDefined();
    }));
});
