import { Component, SimpleChange, Input, Output, EventEmitter, ViewChild, ElementRef, OnChanges, ViewEncapsulation } from '@angular/core';

import {EmitterService} from '../../../core/services/emitter.service';

import { SortCriteria } from '../../../../common/models/sort-criteria.model';

@Component({
    selector: 'gl-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.less']
})
export class TableComponent implements OnChanges {
    @ViewChild('tbody') private tbodyContainer: ElementRef;
    @Input() studentRecords: any;
    @Input() totalPage = 0;
    @Output() onSort = new EventEmitter<SortCriteria>();
    @Output() onScroll = new EventEmitter<number>();

    private lazyPoint = 0; // point to call next request
    private offset = 1; // number of page
    private isLoading = false; // prevent more than 1 request

    constructor() {
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        for (const propName in changes) {
            if (propName === 'studentRecords') {
                const changedProp = changes[propName];
                if (changedProp.currentValue
                    && changedProp.currentValue.length > 0
                    && changedProp.currentValue > changedProp.previousValue) {
                    this.calcLazyPoint();
                } else {
                    this.tbodyContainer.nativeElement.scrollTop = 0;
                    this.lazyPoint = 400;
                    this.offset = 1;
                    this.isLoading = false;
                }
            }
        }
    }

    private calcLazyPoint() {
        this.lazyPoint = this.tbodyContainer.nativeElement.scrollHeight + 200;
        this.isLoading = false;
    }

    private getTableData() {
        this.offset += 1; // increase page size
        this.onScroll.emit(this.offset);
    }

    scroll() {
        const top = this.tbodyContainer.nativeElement.scrollTop; // get scrollTop
        const height = this.tbodyContainer.nativeElement.offsetHeight; // viewport height
        const scrollPoint = top + height;
        if (scrollPoint > this.lazyPoint && !this.isLoading && this.offset < this.totalPage) {
            this.isLoading = true; // prevent more than 1 request
            this.getTableData(); // ajax request
        }
    }

    detail(studentId, courseName, teacherId, teacherName) {
        EmitterService.get('popup').emit({ studentId: studentId, courseName: courseName, teacherName: teacherName, teacherId: teacherId});
    }

    sort(key: string, sortBy: string) {
        this.offset = 1; // increase page size
        const sortCriteria: SortCriteria = { orderBy: key, sort: sortBy };
        this.onSort.emit(sortCriteria);
    }
}
