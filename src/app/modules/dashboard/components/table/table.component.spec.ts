import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { TableComponent } from './table.component';
import { DataErrorComponent } from '../../../shared/components/data-error/data-error.component';
import { LoadingComponent } from '../../../shared/components/loading/loading.component';

import { StudentDataService } from '../../../core/services/student-data.service';
import { EmitterService } from '../../../core/services/emitter.service';
import { RESTService } from '../../../core/services/rest.service';
import { Utils } from '../../../core/services/utils.service';
import { PluralizePipe } from '../../../shared/pipes/pluralize.pipe';

describe('TableComponent', () => {
    let component: TableComponent;
    let fixture: ComponentFixture<TableComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [TableComponent, DataErrorComponent, LoadingComponent],
            providers: [StudentDataService, RESTService, EmitterService, Utils, PluralizePipe]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TableComponent);
        component = fixture.componentInstance;
        component.studentRecords = [
            {
                'totalStudent': 2,
                'studentid': 'b0f5d029-4d75-47ba-8774-6ca6e16cdf1c',
                'name': 'James Murray',
                'subject': 'KCLA White Fire and Emergency Services',
                'currentScore': 52,
                'overdue': 3,
                'courseProgress': 90
            },
            {
                'totalStudent': 2,
                'studentid': '8774-4d75-47ba-8774-6ca6e16cdf1c',
                'name': 'James Murray 2',
                'subject': 'KCLA White Fire and Emergency Services',
                'currentScore': 52,
                'overdue': 3,
                'courseProgress': 90
            }];
        component.totalPage = 5;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should emit scroll event', (done) => {
        component.onScroll.subscribe(n => {
            expect(n).toEqual(2);
            done();
        });
        component.scroll();
    });

    it('should emit sort event', (done) => {
        const key = 'x';
        const sortBy = 'y';
        component.onSort.subscribe(v => {
            expect(v).toEqual({ orderBy: key, sort: sortBy });
            done();
        });
        component.sort(key, sortBy);
    });
});
