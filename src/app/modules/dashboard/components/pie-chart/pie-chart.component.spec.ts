import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { PieChartComponent } from './pie-chart.component';
import { DataErrorComponent } from '../../../shared/components/data-error/data-error.component';
import { LoadingComponent } from '../../../shared/components/loading/loading.component';

import { StudentDataService } from '../../../core/services/student-data.service';
import { EmitterService } from '../../../core/services/emitter.service';
import { RESTService } from '../../../core/services/rest.service';
import { Utils } from '../../../core/services/utils.service';
import { PluralizePipe } from '../../../shared/pipes/pluralize.pipe';

describe('PieChartComponent', () => {
    let component: PieChartComponent;
    let fixture: ComponentFixture<PieChartComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [PieChartComponent, DataErrorComponent, LoadingComponent],
            providers: [StudentDataService, RESTService, EmitterService, Utils, PluralizePipe]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PieChartComponent);
        component = fixture.componentInstance;
        component.data = [
            { 'label': '5+ Overdue', 'filter': 'overdueplus5', 'overduePercentage': 64, 'studentCount': 35 },
            { 'label': '1-4 Overdue', 'filter': 'overduebetween1to4', 'overduePercentage': 1, 'studentCount': 1 },
            { 'label': '0 Overdue', 'filter': 'overduezero', 'overduePercentage': 33, 'studentCount': 18 }];
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('chart should have `data`', (done) => {
        fixture.detectChanges();
        const chart = fixture.debugElement.query(By.css('.donut-chart')).nativeElement;
        done();
        expect($(chart).find('.arc')).toBeDefined();
    });

    ////it('chart should highlight the `data`', (done) => {
    ////    fixture.detectChanges();
    ////    const chart = fixture.debugElement.query(By.css('.donut-chart')).nativeElement;
    ////    done();
    ////    $(chart).find('.arc path').eq(0).click();
    ////    debugger;
    ////    expect($(chart).find('.arc')).toBeDefined();
    ////});
});
