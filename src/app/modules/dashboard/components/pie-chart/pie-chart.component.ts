import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Input, ViewEncapsulation} from '@angular/core';
import * as d3 from 'd3';

import { EmitterService } from '../../../core/services/emitter.service';
import { PluralizePipe } from '../../../shared/pipes/pluralize.pipe';

@Component({
    selector: 'gl-pie-chart',
    templateUrl: './pie-chart.component.html',
    styleUrls: ['./pie-chart.component.less'],
    encapsulation: ViewEncapsulation.None
})
export class PieChartComponent implements OnInit, OnDestroy {
    @ViewChild('donut') private chartContainer: ElementRef;
    @Input() data: Array<any> = [];

    private subscriptions: Array<any> = [];
    private pie: any;
    private margin: any = { top: 20, right: 20, bottom: 20, left: 20 };
    private width: number;
    private height: number;
    private radius: number;
    private donutWidth: number = 35;
    private element: any;
    private tooltip: any;

    constructor(private pluralize: PluralizePipe) { }

    ngOnInit() {
        this.subscriptions.push(EmitterService.get('clearFilter').subscribe(value => this.clearFilter(value)));
        this.subscriptions.push(EmitterService.get('clearPieFilter').subscribe(value => this.clearFilter(value)));
        this.createChart(this.data);
    }

    ngOnDestroy() {
        if (this.subscriptions) {
            this.subscriptions.forEach(s => s.unsubscribe());
        }
    }

    clearFilter(value) {
        if (value) {
            this.createChart(this.data);
        }
    }

    update(x) {
        this.createChart([this.data[x]], true, x);
    }

    remove() {
        d3.select('.donut-chart svg').remove();
        d3.select('.tool').remove();
    }

    getColor(filter): string {
        const color = ['#CC3834', '#FDC23A', '#2CB364'];
        if (filter === 'overdueplus5') {
            return color[0];
        } else if (filter === 'overduebetween1to4') {
            return color[1];
        } else {
            return color[2];
        }
    }

    svg() {
        let s = d3.select('.donut-chart')
            .append('svg')
            .attr('width', this.element.offsetWidth)
            .attr('height', this.element.offsetHeight)
            .attr('class', 'svgChartElement');

        const title = this.addH4TextElement();
        const line = this.addH4LineElement();
        const pathIconEle = this.addingIconPath();

        s = s.append('g')
            .attr('transform', 'translate(' + ((this.element.offsetWidth / 4) + 25) +
            ',' + ((this.element.offsetHeight / 2)) + ')');

        const defs = s.append('svg:defs');
        this.shadow(defs);
        const legend = this.legend(s);

        return s;
    }

    showTooltip() {
        const t = d3.select('.donut-chart')
            .append('div')
            .attr('class', 'tool');

        t.append('div')
            .attr('class', 'label');

        return t;
    }

    addH4TextElement() {
        const h4Text = d3.selectAll('.svgChartElement');
        const text = h4Text.append('text')
            .attr('x', 132)
            .attr('y', -20)
            .attr('fill', '#4a4a4a')
            .attr('font-family', 'Helvetica Neue, Arial, sans-serif')
            .text('Assignment Completion')
            .attr('font-size', '20px')
            .attr('font-weight', 'bold');
        return h4Text;
    }

    addH4LineElement() {
        const h4Line = d3.selectAll('.svgChartElement');
        const text = h4Line.append('line')
            .attr('stroke', '#cccccc')
            .attr('stroke-width', '0.5')
            .attr('x1', '0')
            .attr('x2', '500')
            .attr('y1', '-5')
            .attr('y2', '-5');
        return h4Line;
    }

    addingIconPath() {
        const h4Path2 = d3.selectAll('.svgChartElement');
        /* tslint:disable:max-line-length */

        const h4PathForIcon = h4Path2.append('path')
            .attr('d', 'M379 -40C373.48 -40 369 -35.519999999999996 369 -30s4.48 10 10 10 10 -4.48 10 -10S384.52 -40 379 -40zm1 15h-2v-6h2v6zm0 -8h-2V-35h2v2z')
            .attr('fill', '#808080');

        /* tslint:enable:max-line-length */
        return h4Path2;
    }

    legend(svg) {
        const legendRectSize = 20;
        const legendSpacing = 10;
        let r;
        if (this.element.offsetWidth >= 540) {
            r = this.radius + 4 * this.donutWidth;
        } else {
            r = this.radius + 3 * this.donutWidth;
        }

        const length = this.data.length;
        const l = svg.selectAll('.legend')
            .data(this.data)
            .enter()
            .append('g')
            .attr('class', 'legend')
            .attr('transform', function (d, i) {
                const height = legendRectSize + legendSpacing;
                const offset = height * length / 2;
                const horz = (-3 * legendRectSize) + r;
                const vert = i * height - offset;
                return 'translate(' + horz + ',' + vert + ')';
            });

        l.append('rect')
            .attr('width', legendRectSize)
            .attr('height', legendRectSize)
            .attr('class', 'legend-btn')
            .attr('rx', 2.5)
            .attr('ry', 2.5)
            .attr('fill', (d, i) => {
                return this.getColor(d.filter);
            })
            .on('click', (d, i) => {
                this.update(i);
                EmitterService.get('overdueFilter').emit(d['filter']);
            })
            .on('mouseover', (d, i, j) => {
                this.handleMouseOver(d);
            })
            .on('mouseout', (d, i, j) => {
                this.handleMouseout(d);
            });

        l.append('text')
            .attr('x', legendRectSize + legendSpacing)
            .attr('y', legendRectSize - legendSpacing + legendRectSize / 4)
            .attr('class', 'legend-text')
            .text(function (d) { return d.label; });

        return l;
    }

    shadow(defs) {
        // Declare shadow filter
        const s = defs.append('filter').attr('id', 'shadow')
            .attr('filterUnits', 'userSpaceOnUse')
            .attr('x', -1 * (this.width / 2)).attr('y', -1 * (this.height / 2))
            .attr('width', this.width).attr('height', this.height);

        // old value = 4
        s.append('feGaussianBlur')
            .attr('in', 'SourceGraphic')
            .attr('stdDeviation', '1')
            .attr('result', 'blur');

        s.append('feOffset')
            .attr('in', 'blur')
            .attr('dx', '1').attr('dy', '1')
            .attr('result', 'offsetBlur');

        s.append('feBlend')
            .attr('in', 'SourceGraphic')
            .attr('in2', 'offsetBlur')
            .attr('mode', 'normal');
    }

    innerText(g) {
        g.append('text')
            .attr('text-anchor', 'middle')
            .attr('dy', '0.35em')
            .attr('font-size', '36px')
            .text(function (d) { return d.value + '%'; });
    }

    outerText(g, arc) {
        g.append('text')
            .attr('transform', (d: any) => {
                const c = arc.centroid(d),
                    x = c[0],
                    y = c[1],
                    // pythagorean theorem for hypotenuse
                    h = Math.sqrt(x * x + y * y);
                return 'translate(' + (x / h * ((this.radius * 1.03) + 10)) + ',' +
                    (y / h * ((this.radius * 1.03) + 10)) + ')';
            })
            .attr('dy', '.35em')
            .attr('text-anchor', function (d) {
                // are we past the center?
                return (d.endAngle + d.startAngle) / 2 > Math.PI ?
                    'end' : 'start';
            })
            .text(function (d) { return d.value + '%'; });
    }

    pathAnim(path, dir) {
        switch (dir) {
            case 0:
                path.transition()
                    .attr('d', d3.arc()
                        .innerRadius((this.radius - this.donutWidth))
                        .outerRadius(this.radius)

                    ).attr('filter', '');
                break;

            case 1:
                path.transition()
                    .attr('d', d3.arc()
                        .innerRadius((this.radius - this.donutWidth) * 0.97)
                        .outerRadius(this.radius * 1.03)
                    ).attr('filter', 'url(#shadow)');
                break;
        }
    }

    handleMouseout(d) {
        const id = (d.data && d.data['filter']) ? d.data['filter'] : d.filter;
        this.pathAnim(d3.select('#' + id), 0);
        this.tooltip.style('display', 'none');
    }

    handleMousemove(d) {
        let coordinates = [0, 0];
        coordinates = d3.mouse(d3.event.currentTarget);
        const x = coordinates[0];
        const y = coordinates[1];
        this.tooltip.style('top', (this.element.offsetHeight / 2 + this.element.offsetTop + y + 10) + 'px')
            .style('left', (this.element.offsetWidth / 4 + this.element.offsetLeft + x + 40) + 'px');
    }

    handleMouseOver(d) {
        const id = (d.data && d.data['filter']) ? d.data['filter'] : d.filter;
        const studentCount = (d.data && d.data['studentCount']) ? d.data['studentCount'] : d.studentCount;
        this.pathAnim(d3.select('#' + id), 1);
        if (d.data) {
            this.tooltip.select('.label').html(this.pluralize.transform(studentCount, 'student', 's'));
            this.tooltip.style('display', 'block');
        }
    }

    chartInit() {
        this.remove();
        this.element = this.chartContainer.nativeElement;
        this.width = this.element.offsetWidth - this.margin.left - this.margin.right;
        this.height = this.element.offsetHeight - this.margin.top - this.margin.bottom;
        if (this.element.offsetWidth >= 540) {
            this.radius = Math.min(this.width - 60, this.height - 60) / 2;
        } else {
            this.radius = Math.min(this.width * 0.9 - 60, this.height * .9 - 60) / 2;
        }
        this.tooltip = this.showTooltip();
    }

    createArrow(svg, index) {
        const legendRectSize = 20;
        const legendSpacing = 10;
        const positions = [-4, -20, -36];
        const ver = positions[this.data.length - 1];
        let r;
        if (this.element.offsetWidth >= 540) {
            r = this.radius + 4 * this.donutWidth;
        } else {
            r = this.radius + 3 * this.donutWidth;
        }

        svg.selectAll('.arrow')
            .data([1])
            .enter().append('g')
            .attr('transform', function (d, i) {
                const horz = (-3 * legendRectSize) + r;
                return 'translate(' + (horz - 15) + ',' + (ver + index * 30) + ') rotate(-270)';
            })
            .append('path')
            .attr('d', d3.symbol().type(d3.symbolTriangle).size(25))
            .attr('fill', '#A4A4A4');
    }

    createChart(inputData, isInnerChart = false, index: number = -1) {
        const data = inputData.filter(x => x.studentCount > 0);
        if (data && data.length > 0) {
            this.chartInit();

            if (this.width <= 0 && this.height <= 0) {
                return;
            }

            const pie = d3.pie()
                .value((d: any) => d.overduePercentage)
                .sort(null);

            const arc = d3.arc()
                .innerRadius(this.radius - this.donutWidth)
                .outerRadius(this.radius);

            const svg = this.svg();

            const g = svg.selectAll('.arc')
                .data(pie(data))
                .enter().append('g')
                .attr('class', 'arc');

            const path = g.append('path')
                .attr('id', function (d, i) {
                    return d.data['filter'];
                })
                .attr('d', <any>arc)
                .attr('fill', (d, i) => {
                    return this.getColor(d.data['filter']);
                })
                .on('mouseover', (d, i, j) => {
                    this.handleMouseOver(d);
                })
                .on('mouseout', (d, i, j) => {
                    this.handleMouseout(d);
                })
                .on('mousemove', (d, i, j) => {
                    this.handleMousemove(d);
                });

            if (!isInnerChart) {
                path.on('click', (d, i) => {
                    this.update(i);
                    EmitterService.get('overdueFilter').emit(d.data['filter']);
                });
            }

            if (isInnerChart) {
                this.innerText(g);
                this.createArrow(svg, index);
            } else {
                this.outerText(g, arc);
            }
        }
    }
}
