import { async, ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { StudentBreakdownComponent } from './student-breakdown.component';
import { TableComponent } from '../table/table.component';
import { DataErrorComponent } from '../../../shared/components/data-error/data-error.component';
import { LoadingComponent } from '../../../shared/components/loading/loading.component';
import { TourComponent } from '../../../shared/components/tour/tour.component';

import { StudentDataService } from '../../../core/services/student-data.service';
import { EmitterService } from '../../../core/services/emitter.service';
import { RESTService } from '../../../core/services/rest.service';
import { Utils } from '../../../core/services/utils.service';

import { FilterCriteria } from '../../../../common/models/filter-criteria.model';
import { SortCriteria } from '../../../../common/models/sort-criteria.model';
import { PluralizePipe } from '../../../shared/pipes/pluralize.pipe';

const DataSetup = {
    singleRecord: [
        {
            'totalStudent': 1,
            'studentid': 'b0f5d029-4d75-47ba-8774-6ca6e16cdf1c',
            'name': 'James Murray',
            'subject': 'KCLA White Fire and Emergency Services',
            'currentScore': 52,
            'overdue': 3,
            'courseProgress': 90
        }],
    multipleRecords: [
        {
            'totalStudent': 2,
            'studentid': 'b0f5d029-4d75-47ba-8774-6ca6e16cdf1c',
            'name': 'James Murray',
            'subject': 'KCLA White Fire and Emergency Services',
            'currentScore': 52,
            'overdue': 3,
            'courseProgress': 90
        },
        {
            'totalStudent': 2,
            'studentid': '8774-4d75-47ba-8774-6ca6e16cdf1c',
            'name': 'James Murray 2',
            'subject': 'KCLA White Fire and Emergency Services',
            'currentScore': 52,
            'overdue': 3,
            'courseProgress': 90
        }]
};

class MockStudentDataService extends StudentDataService {
    public getStudentBreakdown(offset: number, filter: FilterCriteria, sort: SortCriteria): Observable<any> {
        return Observable.of(DataSetup.singleRecord);
    }
}

describe('StudentBreakdownComponent', () => {
    let component: StudentBreakdownComponent;
    let fixture: ComponentFixture<StudentBreakdownComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [StudentBreakdownComponent, TableComponent, DataErrorComponent, LoadingComponent, TourComponent, PluralizePipe],
            providers: [{ provide: StudentDataService, useClass: MockStudentDataService }, RESTService, EmitterService, Utils]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StudentBreakdownComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    ////it('should inject `StudentDataService`', inject([StudentDataService], (studentDataService: StudentDataService) => {
    ////    expect(studentDataService.getStudentBreakdown({ orderBy: 'ASC', sort: 'subject' })).toBe('Injected Service');
    ////});

    it('sort should call `StudentDataService.getStudentBreakdown`', () => {
        const studentDataService = TestBed.get(StudentDataService);
        spyOn(studentDataService, 'getStudentBreakdown').and.returnValue(Observable.of(DataSetup.singleRecord));
        component.sort({ orderBy: 'ASC', sort: 'subject' });
        expect(studentDataService.getStudentBreakdown).toHaveBeenCalled();
        expect(studentDataService.getStudentBreakdown).toHaveBeenCalledWith(
            1,
            { student: '', overdue: '', score: '' },
            { orderBy: 'ASC', sort: 'subject' }, '', '');
    });

    ////it('search should enable clear filter', fakeAsync(() => {
    ////    const studentDataService = TestBed.get(StudentDataService);
    ////    spyOn(studentDataService, 'getStudentBreakdown').and.returnValue(Observable.of(DataSetup.multipleRecords));
    ////    const search = fixture.debugElement.query(By.css('#search')).nativeElement;
    ////    search.value = 'test';
    ////    fixture.detectChanges();
    ////    tick(450); 

    ////    let button = fixture.debugElement.query(By.css('#tourStep4')).nativeElement;
    ////    expect(button).toBeDefined();
    ////    const clearFilter = fixture.debugElement.query(By.css('#clearFilter')).nativeElement;
    ////    clearFilter.click();
    ////    fixture.detectChanges();
    ////    button = fixture.debugElement.query(By.css('#tourStep4'));
    ////    expect(button).toBeNull();
    ////}));

    it('should have plural noun', (done) => {
        const studentDataService = TestBed.get(StudentDataService);
        spyOn(studentDataService, 'getStudentBreakdown').and.returnValue(Observable.of(DataSetup.multipleRecords));
        component.ngOnInit();
        fixture.detectChanges();
        done();
        const totalStudent = fixture.debugElement.query(By.css('.total-student')).nativeElement;
        expect(totalStudent.innerHTML).toBe('2 Results');
    });

    it('should have singular noun', fakeAsync(() => {
        const studentDataService = TestBed.get(StudentDataService);
        spyOn(studentDataService, 'getStudentBreakdown').and.returnValue(Observable.of(DataSetup.singleRecord));
        component.ngOnInit();
        fixture.detectChanges();
        const totalStudent = fixture.debugElement.query(By.css('.total-student')).nativeElement;
        expect(totalStudent.innerHTML).toBe('1 Result');
    }));

    it('export button should exist', () => {
        const clearFilter = fixture.debugElement.query(By.css('#tourStep7')).nativeElement;
        expect(clearFilter).toBeDefined();
    });
});
