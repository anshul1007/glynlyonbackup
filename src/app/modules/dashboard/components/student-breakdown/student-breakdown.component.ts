import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Input } from '@angular/core';

import * as Rx from 'rxjs/Rx';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

import { EmitterService } from '../../../core/services/emitter.service';
import { StudentDataService } from '../../../core/services/student-data.service';
import { Utils } from '../../../core/services/utils.service';

import { FilterCriteria } from '../../../../common/models/filter-criteria.model';
import { SortCriteria } from '../../../../common/models/sort-criteria.model';

declare const canvg: any;

@Component({
    selector: 'gl-student-breakdown',
    templateUrl: './student-breakdown.component.html',
    styleUrls: ['./student-breakdown.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentBreakdownComponent implements OnInit {
    @Input() activeTour: boolean;

    studentRecords: any;
    private typeAheadEventEmitter = new Rx.Subject<string>();
    private filter: FilterCriteria;
    private sortCriteria: SortCriteria;
    private errorOccurred: boolean;
    private isLoading: boolean;
    private tourStatus;
    private offset: number = 1;
    private totalStudent: number = 0;
    private totalPage: number = 0;
    private campusId: string = '';
    private teacherId: string = '';

    constructor(private getData: StudentDataService,
        private changeDetectorRef: ChangeDetectorRef) {

        this.studentRecords = [];
        this.filter = { student: '', overdue: '', score: '' };
        this.sortCriteria = null;
        this.errorOccurred = false;
        this.isLoading = true;
        this.activeTour = false;

        this.typeAheadEventEmitter
            .debounceTime(400)
            .switchMap(() => {
                return this.getData.getStudentBreakdown(this.offset, this.filter, this.sortCriteria, this.campusId, this.teacherId);
            }).subscribe(result => {
                this.errorOccurred = false;
                this.isLoading = false;
                this.studentRecords = result;
                if (result.length !== 0) {
                    this.totalStudent = result[0].totalStudent;
                    this.totalPage = Math.ceil((result[0].totalStudent) / 10);
                } else {
                    this.totalStudent = 0;
                    this.totalPage = 0;
                }
                this.changeDetectorRef.markForCheck();
            }, error => {
                this.errorOccurred = true;
                this.isLoading = false;
                this.studentRecords = [];
                this.changeDetectorRef.markForCheck();
            });

    }

    ngOnInit() {
        this.getResult();
        EmitterService.get('overdueFilter').subscribe(value => this.overdueFilter(value));
        EmitterService.get('scoreFilter').subscribe(value => this.scoreFilter(value));
        EmitterService.get('adminSearch').subscribe(value => this.adminSearch(value));
        document.addEventListener('touchstart', function () { }, true);
    }

    private getResult(): void {
        this.getData.getStudentBreakdown(this.offset, this.filter, this.sortCriteria, this.campusId, this.teacherId)
            .subscribe(result => {
                this.errorOccurred = false;
                this.isLoading = false;
                if (result.length !== 0) {
                    this.totalStudent = result[0].totalStudent;
                    this.totalPage = Math.ceil((result[0].totalStudent) / 10);
                } else {
                    this.totalStudent = 0;
                    this.totalPage = 0;
                }
                if (this.offset > 1) {
                    this.studentRecords = this.studentRecords.concat(result);
                } else {
                    this.studentRecords = result;
                }
                this.changeDetectorRef.markForCheck();
            }, error => {
                this.errorOccurred = true;
                this.isLoading = false;
                this.studentRecords = [];
                this.changeDetectorRef.markForCheck();
            });
    }

    private overdueFilter(value) {
        EmitterService.get('clearBarFilter').emit(true);
        this.filter.score = '';
        this.filter.overdue = value;
        this.offset = 1;
        this.getResult();
    }

    private scoreFilter(value) {
        EmitterService.get('clearPieFilter').emit(true);
        this.filter.score = value;
        this.filter.overdue = '';
        this.offset = 1;
        this.getResult();
    }

    private adminSearch(value) {
        this.campusId = value.campusId;
        this.teacherId = value.teacherId;
        this.clearFilter(false);
    }

    private clearFilter(isStudent: boolean) {
        if (isStudent) {
            this.filter.student = '';
        } else {
            this.filter = { student: '', overdue: '', score: '' };
            EmitterService.get('clearFilter').emit(true);
        }
        this.offset = 1;
        this.getResult();
    }

    private getResultByStudent() {
        this.offset = 1;
        this.isLoading = true;
        this.typeAheadEventEmitter.next();
    }

    sort(sortCriteria: SortCriteria) {
        this.offset = 1;
        this.isLoading = true;
        this.sortCriteria = sortCriteria;
        this.getResult();
    }

    getDataOnScroll(offset) {
        if (this.totalPage > this.offset) {
            this.offset = offset;
            this.getResult();
        }
    }

    getCSV() {
        this.getData.getFiles(this.filter, this.sortCriteria, this.campusId, this.teacherId).subscribe(result => {
            if (result && result._body) {
                Utils.downloadCsvFile('StudentRecords.csv', result._body, null);
            }
        }, error => {
        });
    }

    pdfDownload() {
        window.scrollTo(0, 0);
        const self = this;

        // For canvas
        const divHeight = document.getElementById('forPDFCanvas').clientHeight;
        const divWidth = document.getElementById('forPDFCanvas').clientWidth;
        const ratio = divHeight / divWidth;

        // For svg elements
        const svgs = document.getElementsByTagName('svg');
        let svg_chart_xml = (new XMLSerializer).serializeToString(svgs[0]);
        let svg_bar_xml = (new XMLSerializer).serializeToString(svgs[1]);

        // To identify browser
        const sUsrAg = navigator.userAgent;

        html2canvas(document.getElementById('forPDFCanvas'), {
            height: divHeight,
            width: divWidth,
        }).then(
            function (canvas) {
                setTimeout(() => {
                    // Use canvg for IE, FF and Safari
                    if (navigator.msSaveBlob || (sUsrAg.indexOf('Firefox') > -1) || (sUsrAg.indexOf('Safari') > -1)) {
                        canvas.height = 500;
                        svg_chart_xml = svg_chart_xml.replace(/(?![<svg])[^>]*/, '');
                        svg_bar_xml = svg_bar_xml.replace(/(?![<svg])[^>]*/, '');
                        canvg(canvas, svg_chart_xml, { offsetX: 10, offsetY: 100, ignoreDimensions: true, ignoreClear: true });
                        canvg(canvas, svg_bar_xml, { offsetX: 620, offsetY: 100, ignoreDimensions: true, ignoreClear: true });
                    }
                    // Use canvas directly for Chrome and other devices
                    const image = canvas.toDataURL('image/png');
                    const doc = new jsPDF(); // using defaults: orientation=portrait, unit=mm, size=A4
                    const width = doc.internal.pageSize.width;
                    const height = ratio * width;
                    doc.addImage(image, 'PNG', 0, 0, width, height);
                    const data = doc.output('blob');
                    const formData = new FormData();
                    formData.append('file', data, 'imagesToServer.pdf');

                    self.getData.getDetailPDF(self.filter, self.sortCriteria, formData, self.campusId, self.teacherId).subscribe(result => {
                        if (result && result._body) {
                            Utils.downloadPdfFile('StudentRecords.pdf', result._body);
                        }

                    }, error => {
                    });
                }, 0);
            });
    }
}
