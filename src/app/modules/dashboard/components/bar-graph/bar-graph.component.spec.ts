import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { BarGraphComponent } from './bar-graph.component';
import { DataErrorComponent } from '../../../shared/components/data-error/data-error.component';
import { LoadingComponent } from '../../../shared/components/loading/loading.component';

import { StudentDataService } from '../../../core/services/student-data.service';
import { EmitterService } from '../../../core/services/emitter.service';
import { RESTService } from '../../../core/services/rest.service';
import { Utils } from '../../../core/services/utils.service';
import { PluralizePipe } from '../../../shared/pipes/pluralize.pipe';

describe('BarGraphComponent', () => {
    let component: BarGraphComponent;
    let fixture: ComponentFixture<BarGraphComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [BarGraphComponent, DataErrorComponent, LoadingComponent],
            providers: [StudentDataService, RESTService, EmitterService, Utils, PluralizePipe]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BarGraphComponent);
        component = fixture.componentInstance;
        component.data = [{ 'range': '0-49%', 'studentCount': 6, 'label': 'scorebetween0to49' },
            { 'range': '50-69%', 'studentCount': 7, 'label': 'scorebetween50to69' }];
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should have `chart`', (done) => {
        fixture.detectChanges();
        const chart = fixture.debugElement.query(By.css('.bar-chart')).nativeElement;
        done();
        expect(chart).toBeDefined();
    });

    it('chart should have `data`', (done) => {
        fixture.detectChanges();
        const chart = fixture.debugElement.query(By.css('.bar-chart')).nativeElement;
        done();
        expect($(chart).find('.mainBar')).toBeDefined();
    });

    it('chart should highlight `data`', (done) => {
        component.highlightGraph(1);
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            const chart = fixture.debugElement.query(By.css('.bar-chart')).nativeElement;
            done();
            expect($(chart).find('.mainBar').eq(0).attr('fill')).toEqual('#b7e8f4');
            expect($(chart).find('.mainBar').eq(1).attr('fill')).toEqual('#1ad4f6');
        });
    });
});
