import {
    Component,
    OnInit,
    ViewChild,
    ElementRef,
    Input,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';

import * as d3 from 'd3';

import { EmitterService } from '../../../core/services/emitter.service';
import { PluralizePipe } from '../../../shared/pipes/pluralize.pipe';

@Component({
    selector: 'gl-bar-graph',
    templateUrl: './bar-graph.component.html',
    styleUrls: ['./bar-graph.component.less'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BarGraphComponent implements OnInit {
    @ViewChild('chart') private chartContainer: ElementRef;
    @Input() data: Array<any> = [];

    private margin: any = { top: 20, bottom: 20, left: 20, right: 20 };
    private chart: any;
    private width: number;
    private height: number;
    private xScale: any;
    private yScale: any;
    private xAxis: any;
    private yAxis: any;

    constructor(private changeDetectorRef: ChangeDetectorRef, private pluralize: PluralizePipe) { }

    ngOnInit() {
        EmitterService.get('clearFilter').subscribe(value => this.clearFilter(value));
        EmitterService.get('clearBarFilter').subscribe(value => this.clearFilter(value));
        this.createChart();
        if (this.data) {
            this.updateChart();
        }
    }

    private clearFilter(value) {
        if (value) {
            this.updateChart();
        }
    }

    private createChart() {
        const element = this.chartContainer.nativeElement;
        this.width = element.offsetWidth - this.margin.left - this.margin.right;
        this.height = element.offsetHeight - this.margin.top - this.margin.bottom;

        const svg = d3.select(element).append('svg')
            .attr('width', element.offsetWidth)
            .attr('height', element.offsetHeight)
            .attr('class', 'svgBarElement');


        const title = this.addH4TextElement();
        const line = this.addH4LineElement();
        const pathIconEle = this.addingIconPath();

        // chart plot area
        this.chart = svg.append('g')
            .attr('class', 'bars')
            .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);

        // define X & Y domains
        const xDomain = this.data.map(d => d.range);
        const yDomain = [0, d3.max(this.data, d => d.studentCount)];

        // create scales
        this.xScale = d3.scaleBand().padding(0.3).domain(xDomain).rangeRound([0, this.width]);
        this.yScale = d3.scaleLinear().domain(yDomain).range([this.height, 0]);

        // x & y axis
        this.xAxis = svg.append('g')
            .attr('class', 'axis axis-x')
            .attr('transform', `translate(${this.margin.left}, ${this.margin.top + this.height})`)
            .call(d3.axisBottom(this.xScale));

        this.yAxis = svg.append('g')
            .attr('class', 'axis axis-y')
            .attr('transform', `translate(${this.margin.left + 5}, ${this.margin.top - 5})`)
            .call(d3.axisLeft(this.yScale));

        this.xScale.domain(this.data.map(d => d.range));
        this.yScale.domain([0, d3.max(this.data, d => d.studentCount) + Math.floor(d3.max(this.data, d => d.studentCount) * 2)]);
        this.xAxis.transition().call(d3.axisBottom(this.xScale));
        this.yAxis.transition().call(d3.axisLeft(this.yScale));

        const backgroundBar = this.chart.selectAll('.backgroundBar')
            .data(this.data);

        // update existing bars
        backgroundBar
            .enter()
            .append('rect')
            .attr('class', 'backgroundBar')
            .attr('fill', (d, i) => {
                return '#ecf0f4';
            })
            .attr('x', d => this.xScale(d.range))
            .attr('y', d => this.yScale(0))
            .attr('width', this.xScale.bandwidth())
            .attr('height', 0)
            .attr('rx', 3)
            .attr('ry', 3)
            .transition()
            .delay((d, i) => i * 10)
            .attr('y', 0)
            .attr('height', this.height);
    }

    addH4TextElement() {
        const h4Text = d3.selectAll('.svgBarElement');
        const text = h4Text.append('text')
            .attr('x', 120)
            .attr('y', -20)
            .attr('fill', '#4a4a4a')
            .attr('font-family', 'Helvetica Neue, Arial, sans-serif')
            .text('Current Score')
            .attr('font-size', '20px')
            .attr('font-weight', 'bold');
        return h4Text;
    }

    addH4LineElement() {
        const h4Line = d3.selectAll('.svgBarElement');
        const text = h4Line.append('line')
            .attr('stroke', '#cccccc')
            .attr('stroke-width', '0.5')
            .attr('x1', '-60')
            .attr('x2', '480')
            .attr('y1', '-5')
            .attr('y2', '-5');
        return h4Line;
    }

    addingIconPath() {
        const h4Path2 = d3.selectAll('.svgBarElement');
        /* tslint:disable:max-line-length */

        const h4PathForIcon = h4Path2.append('path')
            .attr('d', 'M271 -39C265.48 -39 261 -34.519999999999996 261 -29s4.48 10 10 10 10 -4.48 10 -10S276.52 -39 271 -39zm1 15h-2v-6h2v6zm0 -8h-2V-34h2v2z')
            .attr('fill', '#808080');

        /* tslint:enable:max-line-length */
        return h4Path2;
    }

    updateChart() {
        const element = this.chartContainer.nativeElement;

        const tooltip = d3.select('.bar-chart').append('div').attr('class', 'tool1');
        tooltip.append('div').attr('class', 'label');

        let mainBar = this.chart.selectAll('.mainBar');

        // remove exiting bars
        mainBar.remove();

        mainBar = this.chart.selectAll('.mainBar');

        // add new bars
        mainBar
            .data(this.data)
            .enter()
            .append('rect')
            .attr('class', 'mainBar')
            .attr('x', d => this.xScale(d.range))
            .attr('y', d => this.yScale(0))
            .attr('width', this.xScale.bandwidth())
            .attr('rx', 3)
            .attr('ry', 3)
            .attr('fill', (d, i) => {
                return '#1ad4f6';
            })
            .on('mouseover', (d, i) => {

                tooltip.select('.label').html(this.pluralize.transform(d.studentCount, 'student', 's'));
                tooltip.style('display', 'block')
                    .style('left', this.xScale(d.range) + element.offsetLeft - 10 + 'px')
                    .style('top', this.yScale(d.studentCount) + element.offsetTop - 30 + 'px');
            })
            .on('mouseout', handleMouseout)
            .on('click', (d, i) => {
                this.highlightGraph(i);
                EmitterService.get('scoreFilter').emit(d.label);
            })
            .transition()
            .delay((d, i) => i * 10)
            .attr('y', d => this.yScale(d.studentCount))
            .attr('height', d => this.height - this.yScale(d.studentCount));

        function handleMouseout(d, i, j) {
            tooltip.style('display', 'none');
        }

        function handleMouseOver(d, i) {
            tooltip.select('.label').html(this.pluralize.transform(d.studentCount, 'student', 's'));
            tooltip.style('display', 'block');
        }
        this.changeDetectorRef.markForCheck();
    }

    highlightGraph(index) {
        const mainBar = this.chart.selectAll('.mainBar')
            .data(this.data)
            .attr('fill', (d, i) => {
                return (i !== index) ? '#b7e8f4' : '#1ad4f6';
            });
    }
}
