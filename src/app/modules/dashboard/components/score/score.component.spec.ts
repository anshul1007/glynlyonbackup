import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { ScoreComponent } from './score.component';
import { BarGraphComponent } from '../bar-graph/bar-graph.component';
import { DataErrorComponent } from '../../../shared/components/data-error/data-error.component';
import { LoadingComponent } from '../../../shared/components/loading/loading.component';

import { StudentDataService } from '../../../core/services/student-data.service';
import { EmitterService } from '../../../core/services/emitter.service';
import { RESTService } from '../../../core/services/rest.service';
import { Utils } from '../../../core/services/utils.service';
import { PluralizePipe } from '../../../shared/pipes/pluralize.pipe';

describe('ScoreComponent', () => {
    let component: ScoreComponent;
    let fixture: ComponentFixture<ScoreComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [ScoreComponent, BarGraphComponent, DataErrorComponent, LoadingComponent],
            providers: [StudentDataService, RESTService, EmitterService, Utils, PluralizePipe]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ScoreComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should have `gl-bar-graph`', () => {
        const studentDataService = TestBed.get(StudentDataService);
        spyOn(studentDataService, 'getCurrentScore').and
            .returnValue(Observable.of([
                { 'range': '0-49%', 'studentCount': 6, 'label': 'scorebetween0to49' },
                { 'range': '50-69%', 'studentCount': 7, 'label': 'scorebetween50to69' }]));
        component.ngOnInit();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            const graph = fixture.debugElement.query(By.css('gl-bar-graph')).nativeElement;
            expect(graph).toBeDefined();
        });
    });
});
