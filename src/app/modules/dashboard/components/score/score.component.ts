import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

import { EmitterService } from '../../../core/services/emitter.service';
import { StudentDataService } from '../../../core/services/student-data.service';

@Component({
    selector: 'gl-score',
    templateUrl: './score.component.html',
    styleUrls: ['./score.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScoreComponent implements OnInit {

    private currentScore: Array<any>;
    private errorOccurred: boolean;
    private isLoading = false;
    private campusId: string = '';
    private teacherId: string = '';

    constructor(private getData: StudentDataService, private changeDetectorRef: ChangeDetectorRef) { }

    ngOnInit() {
        this.getResult();
        document.addEventListener('touchstart', function () { }, true);
        EmitterService.get('adminSearch').subscribe(value => this.adminSearch(value));
    }

    private adminSearch(value) {
        this.campusId = value.campusId;
        this.teacherId = value.teacherId;
        this.getResult();
    }

    getResult() {
        this.isLoading = true;
        this.getData.getCurrentScore(this.campusId, this.teacherId).subscribe(
            (data) => {
                this.errorOccurred = false;
                this.isLoading = false;
                this.currentScore = data;
                this.changeDetectorRef.markForCheck();
            },
            (error) => {
                this.errorOccurred = true;
                this.isLoading = false;
                this.changeDetectorRef.markForCheck();
            });
    }
}
