import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    ViewChild,
    AfterViewInit } from '@angular/core';

import { EmitterService } from '../core/services/emitter.service';
import { TourComponent } from '../shared/components/tour/tour.component';
import { StudentDataService } from '../core/services/student-data.service';
import { AdminSearch } from '../../common/models/admin-search';

@Component({
    selector: 'gl-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.less'],
    changeDetection: ChangeDetectionStrategy.Default
})
export class DashboardComponent implements OnInit, AfterViewInit {
    @ViewChild('myTour') tour: TourComponent;
    private showDetails: boolean = false;
    private defaultTeacherId: string = '';
    private studentId: string;
    private courseName: string;
    private tourSteps = [];
    private tourStatus: boolean = false;
    private isAdmin: boolean = false;
    private campusTeacherList = [];
    private teacherList = [];
    private allCampusList = [];
    private allTeacherList = [];
    private adminSearch: AdminSearch;
    private teacherName: string;
    private studentTeacherId: string;

    constructor(private changeDetectorRef: ChangeDetectorRef, private getData: StudentDataService) { }

    ngOnInit() {
        EmitterService.get('popup').subscribe(data => this.isClicked(data));
        this.initTourSteps();
        this.checkUserRole();
        this.adminSearch = {
            campus: '',
            teacher: '',
            onlyMyStudent: false
        };
    }

    private tourStatusChange(val) {
        setTimeout(() => {
            this.tourStatus = val;
            this.changeDetectorRef.detectChanges();
        }, 1);
    }

    private checkUserRole() {
        this.isAdmin = false;
        this.getData.getUserRole().subscribe(userDeatils => {
            if (userDeatils && userDeatils[0]) {
                this.defaultTeacherId = userDeatils[0].userId;
                if (userDeatils[0].userType === 'ADMIN') {
                    this.getData.getCampusTeacherList().subscribe(result => {
                        this.isAdmin = true;
                        this.allCampusList = result;
                        this.campusTeacherList = result;
                        this.allTeacherList = this.removeDuplicate([].concat.apply([],
                            result.map((obj) => {
                                return obj.teacherList;
                            })));
                        this.teacherList = this.allTeacherList;
                    }, error => {
                        console.log(error);
                    });
                }
            }
        }, error => {
            console.log(error);
        });
    }

    private removeDuplicate(array: Array<Object>) {
        const noDupeObj = {};
        const n = array.length;
        for (let i = 0; i < n; i++) {
            const item = array[i];
            noDupeObj[item['teacherId'] + '|' + item['teacherName']] = item;
        }
        let i = 0;
        const nonDuplicatedArray = [];
        for (const item in noDupeObj) {
            if (noDupeObj.hasOwnProperty(item)) {
                nonDuplicatedArray[i++] = noDupeObj[item];
            }
        }
        return nonDuplicatedArray.sort(function (a, b) {
            const x = a.teacherName.toLowerCase();
            const y = b.teacherName.toLowerCase();
            return x < y ? -1 : x > y ? 1 : 0;
        });
    }

    private tourAgain() {
        if (this.tour) {
            localStorage.removeItem('tour_page2_status');
            this.tour.start(2);
        }
    }

    ngAfterViewInit() {
        if (this.tour) {
            this.tour.start(1);
        }
    }

    private initTourSteps() {
        /* tslint:disable:max-line-length */

        this.tourSteps = [
            {
                onShow: function () {
                    this.access = true;
                    this.tourIntro = true;
                },
                onHide: function () {
                    this.tourIntro = false;
                },
                element: '#tourStep1',
                title: 'Welcome to the Teacher Dashboard',
                content: 'As a first-time visitor, this tutorial will walk you through the most important elements of the dashboard. Click \'next\' to start the short tour or \'end\' to stop at any time.',
                smartPlacement: true,
                backdrop: true
            },
            {
                element: '#tourStep2',
                title: 'Overdue Assignments',
                smartPlacement: true,
                content: 'Click any color on the chart to view the students that fall into that overdue assignment category.',
                backdrop: true
            },
            {
                element: '#tourStep3',
                title: 'Current Score',
                smartPlacement: true,
                content: 'Click any bar on the chart to view the students that fall into that score category.',
                backdrop: true
            },
            {
                element: '#tourStep4',
                title: 'Clear Filter',
                smartPlacement: true,
                content: 'Click here to clear the filter and return to your full student list.',
                backdrop: true
            },
            {
                element: '#tourStep5',
                title: 'Student/ Course Search',
                smartPlacement: true,
                content: 'You can search for a student or filter for a specific course here.',
                backdrop: true
            },
            {
                element: '#tourStep6',
                title: 'Sort',
                smartPlacement: true,
                content: 'Click the up/down arrows to sort the student list by name, course, score, overdue or progress.',
                backdrop: true
            },
            {
                element: '#tourStep7',
                title: 'Export',
                smartPlacement: true,
                content: 'Click here to export your student information as a .csv or .pdf.',
                backdrop: true
            },
            {
                element: '#tourStep8',
                title: 'Student Detail',
                smartPlacement: true,
                content: 'Click here to launch the student detail view to get more information on each student.',
                backdrop: true,
            }];

        /* tslint:enable:max-line-length */
    }

    private onCampusChange(newObj) {
        this.adminSearch.campus = newObj;
        if (this.adminSearch.campus === '' && this.adminSearch.teacher === '') {
            this.adminSearch.teacher = '';
            this.teacherList = this.allTeacherList;
        } else if (this.adminSearch.campus !== '' && this.adminSearch.teacher === '') {
            this.adminSearch.teacher = '';
            this.teacherList = newObj.teacherList || [];
        }
        this.onAdminSearch();
    }

    private onTeacherChange(newObj) {
        this.adminSearch.teacher = newObj;
        if (this.adminSearch.teacher === '' && this.adminSearch.campus === '') {
            this.adminSearch.campus = '';
            this.campusTeacherList = this.allCampusList;
        } else if (this.adminSearch.teacher !== '' && this.adminSearch.campus === '') {
            this.adminSearch.campus = '';
            this.campusTeacherList = this.allCampusList.filter(x => {
                return x.teacherList.find(myObj => myObj.teacherId === this.adminSearch.teacher['teacherId']);
            }) || [];
        }
        this.onAdminSearch();
    }

    private onAdminSearch() {
        if (this.adminSearch.onlyMyStudent) {
            EmitterService.get('adminSearch').emit({ campusId: this.adminSearch.campus['campusId'], teacherId: this.defaultTeacherId });
        } else {
            EmitterService.get('adminSearch').emit({
                campusId: this.adminSearch.campus['campusId'],
                teacherId: this.adminSearch.teacher['teacherId']
            });
        }
    }

    private isClicked(data: any) {
        this.studentId = data.studentId;
        this.courseName = data.courseName;
        this.teacherName = data.teacherName;
        this.studentTeacherId = data.teacherId;
        this.showDetails = data.studentId !== '';
        this.changeDetectorRef.markForCheck();
    }

    private toggleMyStudents() {
        this.adminSearch.campus = '';
        this.adminSearch.teacher = '';
        this.onAdminSearch();
    }
}
