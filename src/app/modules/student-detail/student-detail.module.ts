﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { SharedModule } from '../shared/shared.module';

import { StudentDetailComponent } from './student-detail.component';

import { RadialScoreComponent } from './components/radial-score/radial-score.component';
import { ScoreByTypeComponent } from './components/score-by-type/score-by-type.component';
import { StudentProgressComponent } from './components/student-progress/student-progress.component';
import { StudentScoringComponent } from './components/student-scoring/student-scoring.component';
import { StudentTimeComponent } from './components/student-time/student-time.component';

@NgModule({
    imports: [
        BrowserModule, SharedModule
    ],
    declarations: [
        StudentDetailComponent,
        StudentScoringComponent,
        StudentTimeComponent,
        StudentProgressComponent,
        RadialScoreComponent,
        ScoreByTypeComponent,
    ],
    exports: [
        StudentDetailComponent
    ]
})

export class StudentDetailsModule {

}
