import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';

import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

import { StudentDataService } from '../core/services/student-data.service';
import { EmitterService } from '../core/services/emitter.service';
import { Utils } from '../core/services/utils.service';

import { StudentDetail } from '../../common/models/student-detail.model';
import { Subject } from '../../common/models/subject.model';

import { TourComponent } from '../shared/components/tour/tour.component';

@Component({
    selector: 'gl-student-detail',
    templateUrl: './student-detail.component.html',
    styleUrls: ['./student-detail.component.less']
})
export class StudentDetailComponent implements OnInit, AfterViewInit {
    @ViewChild('myTour2') tour: TourComponent;

    private studentDetails: StudentDetail;
    private subjects;
    private multipleSubjects = false;
    private selectedSubject: number = -3;
    private currentScore: number;
    private relativeScore: number;
    private progress: number;
    private overdue: number;
    private courseEndDate: string;
    private totalAssignment: number;
    private totalAssignmentCompleted: number;
    private isLoading: boolean;
    private errorOccurred: boolean;
    private activeSubject: Subject;
    private tourSteps = [];

    @Input() studentId: string;
    @Input() courseName: string;
    @Input() teacherName: string;
    @Input() teacherId: string;
    @Input() isAdmin: boolean;

    constructor(private getData: StudentDataService) { }

    ngOnInit() {
        this.studentDetails = new StudentDetail();
        this.getResult();
        this.initTourSteps();
    }

    ngAfterViewInit() {
        if (this.tour) {
            this.tour.start(2);
        }
    }

    initTourSteps() {
        /* tslint:disable:max-line-length */

        this.tourSteps = [
            {
                element: '#tourStep9',
                title: 'Welcome to the Student Detail Page',
                smartPlacement: true,
                content: 'As a first-time visitor, this tutorial will walk you through the most important elements of the student detail page. Click \'next\' to start the short tour or \'end\' to stop at any time.',
                backdrop: true
            },
            {
                element: '#tourStep10',
                title: 'Multiple courses',
                smartPlacement: true,
                content: 'Click each course to flip between course detail pages (only appears if student is enrolled in more than one course',
                backdrop: true
            },
            {
                element: '#tourStep11',
                title: 'Definitions',
                smartPlacement: true,
                content: 'Hover over any info icon for more information',
                backdrop: true
            },
            {
                element: '#tourStep12',
                title: 'Score by Type',
                smartPlacement: true,
                content: 'Hover over each type box for quick info on avg. attempts and avg. time per attempt.',
                backdrop: true
            },
            {
                element: '#tourStep13',
                title: 'Gradebook',
                smartPlacement: true,
                content: 'Click the Gradebook button to go straight to the student\'s gradebook for more detailed course progress information.',
                backdrop: true
            },
            {
                element: '#tourStep14',
                title: 'Export',
                smartPlacement: true,
                content: 'Click here to export your student information as a .csv or .pdf.',
                backdrop: true
            },
            {
                element: '#tourStep15',
                title: 'Close Detail',
                smartPlacement: true,
                content: 'Click here to close the detail screen and return to the main dashboard screen.',
                backdrop: true
            }];
        /* tslint:enable:max-line-length */

    }

    tourAgain() {
        if (this.tour) {
            localStorage.removeItem('tour_page2_status');
            this.tour.start(2);
        }
    }

    getResult() {
        this.isLoading = true;
        this.errorOccurred = false;
        this.getData.getStudentDetail(this.studentId, this.teacherId).subscribe(result => {
            this.studentDetails = result[0];
        });

        this.getData.getStudentSubjects(this.studentId, this.teacherId).subscribe(result => {
            this.subjects = result;

            this.multipleSubjects = (this.subjects.length > 1);

            this.activeSubject = result.find(x => x.courseName === this.courseName);

            if (this.activeSubject) {
                this.selectedSubject = this.activeSubject.id;
                this.getStudentProgress();
            } else {
                this.selectedSubject = -1;
                this.errorOccurred = false;
                this.isLoading = false;
            }
        }, error => {
            this.selectedSubject = -2;
            this.errorOccurred = true;
            this.isLoading = false;
        });
    }

    selectSubject(id, subject: Subject) {
        if (id !== this.selectedSubject) {
            this.selectedSubject = id;
            this.getStudentProgress();
        }
        this.activeSubject = subject;
    }

    getStudentProgress() {
        this.isLoading = true;
        this.errorOccurred = false;
        this.getData.getStudentProgress(this.studentId, this.selectedSubject, this.teacherId).subscribe(result => {
            if (result && result.length > 0) {
                this.currentScore = result[0].score;
                this.relativeScore = result[0].relativeScore;
                this.progress = result[0].progress;
                this.overdue = result[0].overdueCount;
                this.courseEndDate = result[0].courseEndDate;
                this.totalAssignment = result[0].totalAssignment;
                this.totalAssignmentCompleted = result[0].totalAssignmentCompleted;
            }
            this.isLoading = false;
        }, error => {
            this.errorOccurred = true;
            this.isLoading = false;
        });
    }

    close() {
        EmitterService.get('popup').emit({ studentId: '', courseName: '', teacherName: '', teacherId: '' });
    }

    getCSV() {
        this.getData.getDetailCSV(this.studentId, this.teacherId).subscribe(result => {
            if (result && result._body) {
                Utils.downloadCsvFile('StudentRecords.csv', result._body, null);
            }
        }, error => {
        });
    }

    pdfDownload() {
        document.getElementById('tourStep14').classList.remove('open');
        window.scrollTo(0, 0);
        const divHeight = document.getElementById('tourStep9').clientHeight;
        const divWidth = document.getElementById('tourStep9').clientWidth;
        const ratio = divHeight / divWidth;
        html2canvas(document.getElementById('tourStep9'), {
            height: divHeight,
            width: divWidth,
        }).then(
            function (canvas) {
                const image = canvas.toDataURL('image/png');
                const doc = new jsPDF();
                const width = doc.internal.pageSize.width;
                const height = ratio * width;
                doc.addImage(image, 'PNG', 0, 0, width, height);
                doc.save('StudentDetail.pdf'); // Download the rendered PDF
            });
    }
}
