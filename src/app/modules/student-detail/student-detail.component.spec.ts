import { async, ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, SimpleChange } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { StudentDetailComponent } from './student-detail.component';
import { RadialScoreComponent } from './components/radial-score/radial-score.component';
import { ScoreByTypeComponent } from './components/score-by-type/score-by-type.component';
import { StudentProgressComponent } from './components/student-progress/student-progress.component';
import { StudentScoringComponent } from './components/student-scoring/student-scoring.component';
import { StudentTimeComponent } from './components/student-time/student-time.component';
import { DataErrorComponent } from '../shared/components/data-error/data-error.component';
import { LoadingComponent } from '../shared/components/loading/loading.component';
import { TourComponent } from '../shared/components/tour/tour.component';

import { StudentDataService } from '../core/services/student-data.service';
import { EmitterService } from '../core/services/emitter.service';
import { RESTService } from '../core/services/rest.service';
import { Utils } from '../core/services/utils.service';

import { FilterCriteria } from '../../common/models/filter-criteria.model';
import { SortCriteria } from '../../common/models/sort-criteria.model';

describe('StudentDetailComponent', () => {
    let component: StudentDetailComponent;
    let fixture: ComponentFixture<StudentDetailComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [StudentDetailComponent, StudentProgressComponent, StudentScoringComponent, StudentTimeComponent,
                RadialScoreComponent, ScoreByTypeComponent, DataErrorComponent, LoadingComponent, TourComponent],
            providers: [StudentDataService, RESTService, EmitterService, Utils]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StudentDetailComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
