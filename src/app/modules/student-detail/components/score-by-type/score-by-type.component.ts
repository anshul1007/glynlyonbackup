import { Component, OnInit, Input, OnChanges, SimpleChange} from '@angular/core';

import { StudentDataService } from '../../../core/services/student-data.service';

@Component({
    selector: 'gl-score-by-type',
    templateUrl: './score-by-type.component.html',
    styleUrls: ['./score-by-type.component.less']
})
export class ScoreByTypeComponent implements OnInit, OnChanges {
    @Input() studentId: string;
    @Input() subjectId: number;
    @Input() errorOccurred: boolean;
    @Input() isLoading: boolean = true;
    @Input() teacherId: string;

    private scoreType: any;

    constructor(private getData: StudentDataService) { }

    ngOnInit() {
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        for (const propName in changes) {
            if (propName === 'subjectId') {
                const changedProp = changes[propName];
                if (changedProp.currentValue && changedProp.currentValue > 0 && changedProp.currentValue !== changedProp.previousValue) {
                    this.isLoading = true;
                    this.getData.getStudentSBT(this.studentId, this.subjectId, this.teacherId).subscribe(result => {
                        this.isLoading = false;
                        this.errorOccurred = false;
                        this.bindResult(result);
                    }, error => {
                        this.errorOccurred = true;
                        this.isLoading = false;
                    });
                } else if (changedProp.currentValue === -3) {
                    this.errorOccurred = false;
                    this.isLoading = true;
                } else if (changedProp.currentValue === -2) {
                    this.errorOccurred = true;
                    this.isLoading = false;
                } else {
                    this.errorOccurred = false;
                    this.isLoading = false;
                }
            }
        }
    }

    bindResult(data: any) {
        this.scoreType = [
            {
                'color': 'blue',
                'label': 'L',
                'averageAttempts': data[0].avgAttempts,
                'averageTime': this.secondsToString(data[0].avgTimeOnTask),
                'progress': data[0].avgScore,
                'threshold': data[0].threshold,
            },
            {
                'color': 'green',
                'label': 'P',
                'averageAttempts': data[1].avgAttempts,
                'averageTime': this.secondsToString(data[1].avgTimeOnTask),
                'progress': data[1].avgScore,
                'threshold': data[1].threshold,
            },
            {
                'color': 'yellow',
                'label': 'Q',
                'averageAttempts': data[2].avgAttempts,
                'averageTime': this.secondsToString(data[2].avgTimeOnTask),
                'progress': data[2].avgScore,
                'threshold': data[2].threshold,
            },
            {
                'color': 'red',
                'label': 'T',
                'averageAttempts': data[3].avgAttempts,
                'averageTime': this.secondsToString(data[3].avgTimeOnTask),
                'progress': data[3].avgScore,
                'threshold': data[3].threshold,
            }
        ];

    }

    secondsToString(seconds) {
        const date = new Date(null);
        date.setSeconds(seconds); // specify value for SECONDS here
        return date.toISOString().substr(11, 8);
    }
}
