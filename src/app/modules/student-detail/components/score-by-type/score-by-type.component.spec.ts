import { async, ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, SimpleChange } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { ScoreByTypeComponent } from './score-by-type.component';
import { DataErrorComponent } from '../../../shared/components/data-error/data-error.component';
import { LoadingComponent } from '../../../shared/components/loading/loading.component';
import { TourComponent } from '../../../shared/components/tour/tour.component';

import { StudentDataService } from '../../../core/services/student-data.service';
import { EmitterService } from '../../../core/services/emitter.service';
import { RESTService } from '../../../core/services/rest.service';
import { Utils } from '../../../core/services/utils.service';

import { FilterCriteria } from '../../../../common/models/filter-criteria.model';
import { SortCriteria } from '../../../../common/models/sort-criteria.model';

describe('GlScoreByTypeComponent', () => {
    let component: ScoreByTypeComponent;
    let fixture: ComponentFixture<ScoreByTypeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [ScoreByTypeComponent, DataErrorComponent, LoadingComponent, TourComponent],
            providers: [StudentDataService, RESTService, EmitterService, Utils]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ScoreByTypeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('sort should call `StudentDataService.getStudentSBT`', () => {
        const studentDataService = TestBed.get(StudentDataService);
        spyOn(studentDataService, 'getStudentSBT').and.returnValue(Observable.of([
            { 'avgAttempts': 11, 'avgTimeOnTask': 12, 'avgScore': 13, 'threshold': 14 },
            { 'avgAttempts': 21, 'avgTimeOnTask': 22, 'avgScore': 23, 'threshold': 24 },
            { 'avgAttempts': 31, 'avgTimeOnTask': 32, 'avgScore': 33, 'threshold': 34 },
            { 'avgAttempts': 41, 'avgTimeOnTask': 42, 'avgScore': 43, 'threshold': 44 },
        ]));
        component.subjectId = 123;
        component.ngOnChanges({
            subjectId: new SimpleChange(null, component.subjectId)
        });
        fixture.detectChanges();
        expect(studentDataService.getStudentSBT).toHaveBeenCalled();
        const totalStudent = fixture.debugElement.queryAll(By.css('.sbt-container .sbt-category'));
        expect(totalStudent.length).toBe(4);
    });
});
