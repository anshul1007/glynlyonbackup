import { async, ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, SimpleChange } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { StudentTimeComponent } from './student-time.component';
import { ScoreByTypeComponent } from '../score-by-type/score-by-type.component';
import { DataErrorComponent } from '../../../shared/components/data-error/data-error.component';
import { LoadingComponent } from '../../../shared/components/loading/loading.component';
import { TourComponent } from '../../../shared/components/tour/tour.component';

import { StudentDataService } from '../../../core/services/student-data.service';
import { EmitterService } from '../../../core/services/emitter.service';
import { RESTService } from '../../../core/services/rest.service';
import { Utils } from '../../../core/services/utils.service';

import { FilterCriteria } from '../../../../common/models/filter-criteria.model';
import { SortCriteria } from '../../../../common/models/sort-criteria.model';


describe('StudentTimeComponent', () => {
    let component: StudentTimeComponent;
    let fixture: ComponentFixture<StudentTimeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [StudentTimeComponent, ScoreByTypeComponent, DataErrorComponent, LoadingComponent, TourComponent],
            providers: [StudentDataService, RESTService, EmitterService, Utils]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StudentTimeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('sort should call `StudentDataService.getStudentLogTime`', () => {
        const studentDataService = TestBed.get(StudentDataService);
        spyOn(studentDataService, 'getStudentLogTime').and.returnValue(Observable.of([]));
        component.subjectId = 123;
        component.ngOnChanges({
            subjectId: new SimpleChange(null, component.subjectId)
        });
        fixture.detectChanges();
        expect(studentDataService.getStudentLogTime).toHaveBeenCalled();
        const totalStudent = fixture.debugElement.queryAll(By.css('.box-value'));
        expect(totalStudent.length).toBe(8);
    });
});
