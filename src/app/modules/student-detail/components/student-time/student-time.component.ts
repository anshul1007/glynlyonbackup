import { Component, OnInit, OnChanges, Input, SimpleChange } from '@angular/core';

import { StudentDataService } from '../../../core/services/student-data.service';

@Component({
    selector: 'gl-student-time',
    templateUrl: './student-time.component.html',
    styleUrls: ['./student-time.component.less']
})
export class StudentTimeComponent implements OnInit, OnChanges {
    @Input() studentId: string;
    @Input() subjectId: number;
    @Input() isLoading: boolean = true;
    @Input() errorOccurred: boolean;
    @Input() teacherId: string;

    private data: any = {};

    constructor(private getData: StudentDataService) { }

    ngOnInit() {
         document.addEventListener('touchstart', function(){}, true);
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        for (const propName in changes) {
            if (propName === 'subjectId') {
                const changedProp = changes[propName];
                if (changedProp.currentValue && changedProp.currentValue > 0 && changedProp.currentValue !== changedProp.previousValue) {
                    this.isLoading = true;
                    this.getData.getStudentLogTime(this.studentId, this.subjectId, this.teacherId).subscribe(result => {
                        this.isLoading = false;
                        this.errorOccurred = false;
                        this.data = result[0] || {};
                    }, error => {
                        this.errorOccurred = true;
                        this.isLoading = false;
                    });
                } else if (changedProp.currentValue === -3) {
                    this.errorOccurred = false;
                    this.isLoading = true;
                } else if (changedProp.currentValue === -2) {
                    this.errorOccurred = true;
                    this.isLoading = false;
                } else {
                    this.errorOccurred = false;
                    this.isLoading = false;
                }
            }
        }
    }
}
