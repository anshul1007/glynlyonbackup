import { async, ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { StudentProgressComponent } from './student-progress.component';
import { RadialScoreComponent } from '../radial-score/radial-score.component';
import { DataErrorComponent } from '../../../shared/components/data-error/data-error.component';
import { LoadingComponent } from '../../../shared/components/loading/loading.component';
import { TourComponent } from '../../../shared/components/tour/tour.component';

import { StudentDataService } from '../../../core/services/student-data.service';
import { EmitterService } from '../../../core/services/emitter.service';
import { RESTService } from '../../../core/services/rest.service';
import { Utils } from '../../../core/services/utils.service';

import { FilterCriteria } from '../../../../common/models/filter-criteria.model';
import { SortCriteria } from '../../../../common/models/sort-criteria.model';

describe('StudentProgressComponent', () => {
    let component: StudentProgressComponent;
    let fixture: ComponentFixture<StudentProgressComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [StudentProgressComponent, RadialScoreComponent, DataErrorComponent, LoadingComponent, TourComponent],
            providers: [StudentDataService, RESTService, EmitterService, Utils]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StudentProgressComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should have `gl-radial-score`', () => {
        const totalStudent = fixture.debugElement.query(By.css('gl-radial-score'));
        expect(totalStudent).toBeDefined();
    });
});
