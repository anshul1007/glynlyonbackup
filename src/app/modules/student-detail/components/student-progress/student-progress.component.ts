import { Component, OnInit, Input } from '@angular/core';

import { StudentDataService } from '../../../core/services/student-data.service';

@Component({
    selector: 'gl-student-progress',
    templateUrl: './student-progress.component.html',
    styleUrls: ['./student-progress.component.less']
})
export class StudentProgressComponent implements OnInit {
    @Input() studentId: string;
    @Input() subjectId: number;
    @Input() progress: number;
    @Input() overdue: number;
    @Input() courseEndDate: string;
    @Input() totalAssignment: number;
    @Input() totalAssignmentCompleted: number;
    @Input() isLoading: boolean;
    @Input() errorOccurred: boolean;
    @Input() teacherId: string;

    constructor(private getData: StudentDataService) { }

    ngOnInit() {
    }

}
