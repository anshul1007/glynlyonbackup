import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'gl-student-scoring',
    templateUrl: './student-scoring.component.html',
    styleUrls: ['./student-scoring.component.less']
})
export class StudentScoringComponent implements OnInit {
    @Input() studentId: string;
    @Input() subjectId: number;
    @Input() currentScore: number;
    @Input() relativeScore: number;
    @Input() isLoading: boolean;
    @Input() errorOccurred: boolean;
    @Input() teacherId: string;

    constructor() { }

    ngOnInit() {
        document.addEventListener('touchstart', function(){}, true);
    }
}
