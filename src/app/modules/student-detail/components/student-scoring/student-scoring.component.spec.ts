import { async, ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { StudentScoringComponent } from './student-scoring.component';
import { RadialScoreComponent } from '../radial-score/radial-score.component';
import { ScoreByTypeComponent } from '../score-by-type/score-by-type.component';
import { DataErrorComponent } from '../../../shared/components/data-error/data-error.component';
import { LoadingComponent } from '../../../shared/components/loading/loading.component';
import { TourComponent } from '../../../shared/components/tour/tour.component';

import { StudentDataService } from '../../../core/services/student-data.service';
import { EmitterService } from '../../../core/services/emitter.service';
import { RESTService } from '../../../core/services/rest.service';
import { Utils } from '../../../core/services/utils.service';

import { FilterCriteria } from '../../../../common/models/filter-criteria.model';
import { SortCriteria } from '../../../../common/models/sort-criteria.model';


describe('StudentScoringComponent', () => {
    let component: StudentScoringComponent;
    let fixture: ComponentFixture<StudentScoringComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [StudentScoringComponent, RadialScoreComponent, ScoreByTypeComponent,
                DataErrorComponent, LoadingComponent, TourComponent],
            providers: [StudentDataService, RESTService, EmitterService, Utils]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StudentScoringComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should have `gl-radial-score`', () => {
        const totalStudent = fixture.debugElement.query(By.css('gl-radial-score'));
        expect(totalStudent).toBeDefined();
    });

    it('should have `gl-score-by-type`', () => {
        const totalStudent = fixture.debugElement.query(By.css('gl-score-by-type'));
        expect(totalStudent).toBeDefined();
    });
});
