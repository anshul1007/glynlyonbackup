import { async, ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { RadialScoreComponent } from './radial-score.component';

describe('GlRadialScoreComponent', () => {
    let component: RadialScoreComponent;
    let fixture: ComponentFixture<RadialScoreComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [RadialScoreComponent]
        })
            .compileComponents();
    }));

    describe('hideFloat = false', () => {
        beforeEach(() => {
            fixture = TestBed.createComponent(RadialScoreComponent);
            component = fixture.componentInstance;
            component.progress = 45.6;
            component.hideFloat = false;
            fixture.detectChanges();
        });

        it('should create', () => {
            expect(component).toBeTruthy();
        });

        it('should have float score', () => {
            const score = fixture.debugElement.query(By.css('.label')).nativeElement;
            expect(score.innerText).toBe('45.6%');
        });


    });

    describe('hideFloat = true', () => {
        beforeEach(() => {
            fixture = TestBed.createComponent(RadialScoreComponent);
            component = fixture.componentInstance;
            component.progress = 45.6;
            component.hideFloat = true;
            fixture.detectChanges();
        });

        it('should have whole number score', () => {
            const score = fixture.debugElement.query(By.css('.label')).nativeElement;
            expect(score.innerText).toBe('45%');
        });
    });
});
