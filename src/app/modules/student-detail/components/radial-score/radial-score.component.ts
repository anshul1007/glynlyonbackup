import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'gl-radial-score',
    templateUrl: './radial-score.component.html',
    styleUrls: ['./radial-score.component.less']
})
export class RadialScoreComponent implements OnInit {
    @Input() size: string;
    @Input() color: string;
    @Input() progress: number;
    @Input() hideFloat: boolean;

    private progressInt;
    private progressfloat;
    constructor() { }

    ngOnInit() {
        this.progressInt = Math.floor(this.progress);
        this.progressfloat = Math.round(((this.progress) % 1) * 10);
    }
}
