﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataErrorComponent } from './components/data-error/data-error.component';
import { LoadingComponent } from './components/loading/loading.component';
import { TourComponent } from './components/tour/tour.component';

import { PluralizePipe } from './pipes/pluralize.pipe';

@NgModule({
    imports: [CommonModule],
    declarations: [
        LoadingComponent,
        DataErrorComponent,
        TourComponent,
        PluralizePipe
    ],
    exports: [
        LoadingComponent,
        DataErrorComponent,
        TourComponent,
        PluralizePipe
    ],
    providers: [PluralizePipe]
})

export class SharedModule {

}
