import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'pluralize' })
export class PluralizePipe implements PipeTransform {
    transform(value: number, additionalText?: string, pluralText?: string): any {
        if (value > 1) {
            return value + ' ' + (additionalText || '') + (pluralText || '');
        } else {
            return value + ' ' + (additionalText || '');
        }
    }
}
