import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'gl-data-error',
    templateUrl: './data-error.component.html',
    styleUrls: ['./data-error.component.less']
})
export class DataErrorComponent implements OnInit {
    @Input() private errorText: string;
    constructor() { }

    ngOnInit() {
    }
}
