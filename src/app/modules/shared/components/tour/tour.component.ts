import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';

declare const Tour: any;

import { EmitterService } from '../../../core/services/emitter.service';

@Component({
    selector: 'gl-tour',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './tour.component.html',
    styleUrls: ['./tour.component.less']
})
export class TourComponent implements OnInit {
    @Input() tourSteps: Array<any>;
    @Output() tourStatusChange = new EventEmitter<boolean>();
    private access: boolean;
    private tourIntro: boolean;
    private tour: any;
    private pageNo: number;

    constructor(private changeDetectorRef: ChangeDetectorRef) {

    }

    ngOnInit() {

    }

    init() {
        this.tour = new Tour({
            steps: this.tourSteps,
            // orphan: true, 
            onStart: (tour) => {
                this.tourStatusChange.emit(true);
                setTimeout(() => {
                    this.access = true;
                    this.changeDetectorRef.detectChanges();
                }, 1);
            },
            onEnd: (tour) => {
                this.tourStatusChange.emit(false);
                setTimeout(() => {
                    this.access = false;
                    this.changeDetectorRef.detectChanges();
                }, 1);
                if (this.pageNo === 1) {
                    localStorage.setItem('tour_page1_status', 'done');
                } else if (this.pageNo === 2) {
                    localStorage.setItem('tour_page2_status', 'done');
                }
            }
        });

        // Initialize the tour
        this.tour.init(true);
    }

    start(pageNo) {
        this.pageNo = pageNo;
        if (this.pageNo === 1 && !localStorage.getItem('tour_page1_status')) {
            localStorage.removeItem('tour_current_step');
            localStorage.removeItem('tour_end');

            this.init();
            // Start the tour
            this.tour.start(true);
        } else if (this.pageNo === 2 && !localStorage.getItem('tour_page2_status')) {
            localStorage.removeItem('tour_current_step');
            localStorage.removeItem('tour_end');

            this.init();
            // Start the tour
            this.tour.start(true);
        }
    }
}
