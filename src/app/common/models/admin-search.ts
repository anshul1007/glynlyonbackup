﻿export interface AdminSearch {
    campus: string;
    teacher: string;
    onlyMyStudent: boolean;
}
