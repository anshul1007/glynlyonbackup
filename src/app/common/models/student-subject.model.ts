﻿export interface StudentSubject {
    _id: string;
    index: number;
    name: string;
    cscore: number;
    rscore: number;
    SBT: Array<any>;
    TIME: any;
    cprogress: any;
}
