﻿export interface Subject {
    id: number;
    overdueCount: number;
    courseName: string;
}
