﻿export interface FilterCriteria {
    student: string;
    overdue: string;
    score: string;
}
