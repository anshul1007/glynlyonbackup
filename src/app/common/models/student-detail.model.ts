﻿import { StudentInfo } from './student-info.model';
import { StudentSubject } from './student-subject.model';

export class StudentDetail {
    student_info: StudentInfo;
    subjects: Array<StudentSubject>;
    constructor() {
        this.student_info = { name: '', id: '' };
        this.subjects = new Array<StudentSubject>();
    }
}
