﻿export interface SortCriteria {
    orderBy: string;
    sort: string;
}
