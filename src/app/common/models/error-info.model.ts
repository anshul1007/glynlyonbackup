﻿export interface ErrorInfo {
    content: any;
    httpStatus: number;
    httpStatusText?: string;
}
